<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ClasificacionesTableSeeder::class);
        $this->call(ClasificacionItemsTableSeeder::class);
/*         $this->call(InconsistenciasTableSeeder::class);
        $this->call(RetenidosTableSeeder::class);
        $this->call(GestionesTableSeeder::class);
        $this->call(CierresPqrTableSeeder::class);
        $this->call(CasosEspecialesTableSeeder::class); */
        $this->call(RolesTableSeeder::class);
    }
}
