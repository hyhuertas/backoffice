<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'idrol' => 1,
                'descripcion' => 'Administrador',
                'rol' => '1',
            ),
            1 => 
            array (
                'idrol' => 2,
                'descripcion' => 'Cambio de Plan',
                'rol' => '2',
            ),
            2 => 
            array (
                'idrol' => 3,
                'descripcion' => 'Evasiones',
                'rol' => '3',
            ),
            3 => 
            array (
                'idrol' => 4,
                'descripcion' => 'Inconsistensias',
                'rol' => '4',
            ),
            4 => 
            array (
                'idrol' => 5,
                'descripcion' => 'Cierre PQR',
                'rol' => '5',
            ),
            5 => 
            array (
                'idrol' => 6,
                'descripcion' => 'Casos Especiales',
                'rol' => '6',
            ),
            6 => 
            array (
                'idrol' => 7,
                'descripcion' => 'Retenidos',
                'rol' => '7',
            ),
            7 => 
            array (
                'idrol' => 8,
                'descripcion' => 'Correo',
                'rol' => '8',
            ),
            8 => 
            array (
                'idrol' => 9,
                'descripcion' => 'Asesor',
                'rol' => '9',
            ),
        ));
        
        
    }
}