<?php

use Illuminate\Database\Seeder;

class CasosEspecialesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('casos_especiales')->delete();
        
        \DB::table('casos_especiales')->insert(array (
            0 => 
            array (
                'backoffice_id' => 16769,
                'numero_caso' => 123,
                'fecha_registro' => '2019-10-10',
                'fecha_cierre' => '2019-10-11',
                'estado' => '1',
                'usuario_generador' => 'HECTOR HUERTAS',
                'usuario_asignado' => 'JUAN SIN MIEDO',
                'observaciones' => 'PRIMER CASO ESPECIAL',
                'respuesta' => 'ESTE VA A SER EL PREIMER CASO ESPECIAL',
                'nombre_cliente' => 'DIOMEDEZ RICARDO',
                'motivo_solicitud' => 'NUEVO ACUERDO',
                'min' => 123465,
                'fecha_comunicacion' => '2019-05-10',
                'custcode' => 'CODE123',
                'user_ac' => 'USER_AC',
                'descripcion_propuesta' => 'PRIMERA DESCRIPCION PROPUESTA',
               
               
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'backoffice_id' => 16769,
                'numero_caso' => 2250,
                'fecha_registro' => '2019-10-10',
                'fecha_cierre' => '2019-10-11',
                'estado' => '1',
                'usuario_generador' => 'HECTOR HUERTAS',
                'usuario_asignado' => 'ANA MIRANDA',
                'observaciones' => 'SEGUNDO CASO ESPECIAL',
                'respuesta' => 'ESTE VA A SER EL SEGUNDO CASO ESPECIAL',
                'nombre_cliente' => 'SANDRA PACHON',
                'motivo_solicitud' => 'SIGUIENTE ACUERDO',
                'min' => 48851,
                'fecha_comunicacion' => '2019-05-10',
                'custcode' => 'CODE8569',
                'user_ac' => 'USER_AC',
                'descripcion_propuesta' => 'SEGUNDA DESCRIPCION PROPUESTA',
               
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}