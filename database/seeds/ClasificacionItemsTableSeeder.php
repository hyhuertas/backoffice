<?php

use Illuminate\Database\Seeder;

class ClasificacionItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificacion_items')->delete();
        
        \DB::table('clasificacion_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item' => 'Tramitado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 1,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item' => 'Rechazado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 1,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'item' => 'Corregido',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 1,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'item' => 'Aplica',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'item' => 'No Aplica',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'item' => 'Si Aplica No Se Realizo Labor Lealtad',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'item' => 'Si Aplica No Se Validaron Condiciones Comerciales Actuales',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'item' => 'Si Aplica Se Recibe Capacitacion En Amabilidad y Cordialidad',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'item' => 'Si Aplica Falta de Concentracion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'item' => 'No Aplica Se Brindo Informacion Correcta',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'item' => 'No Aplica El Consultor No Tiene Ingresos En La Cuenta Del Cliente',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'item' => 'No Aplica Se Realizo Solicitud Correcta',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'item' => 'No Aplica Consultor No Ingreso A La Cuenta',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'item' => 'No Aplica Consultor No Pertenece A Esta Ubicacion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'item' => 'Si Aplica Inconsistencia Mal Reportada, Usuario Pertenece A Poliedro',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'item' => 'Si Aplica Usuario Mal Digitado, Se Envia Formato Con User Correcto',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'item' => 'Si Aplica Custcode Mal Digitado, Se Envia Formato Con Custcode Correcto',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'item' => '01-Cambio de Plan Errado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'item' => '02-Evasion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'item' => '03-Activacion Servicio Con Cobro',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'item' => '04-Retenido No Efectivo',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'item' => '05-Prorrateo',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'item' => '06-Down Grade Sin Aval',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'item' => '07-Ofrece Cruce de Min Pero No Realiza Debida Gestion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'item' => '08-Fraude',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'item' => '09-No Desactiva servicio Ofrecidos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'item' => '10-Informacion Errada OCC',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'item' => '11-Informacion Errada Elegidos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'item' => '12-Ajuste Pendiente No Aplicado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'item' => '13-No Informa Cobros A Futuro',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'item' => '14-Informacion Errada Fecha de Paso A Prepago',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'item' => '15-Acuerdo De Pago',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'item' => '16-Activa Bono Con Costo',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'item' => '17-Cambio de Min',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'item' => '18-No Tramita Cambio De Plan',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'item' => '19-No Escala Suspension Temporal',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'item' => '20-Informacion Errada Condiciones Comerciales',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'item' => '21-Inconsistencia Familia Y Amigos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'item' => '22-No Escala Solicitud de Ajuste',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'item' => '23-No Bloquea APN',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'item' => '24-Genera Cambio de Plan Sin Autorizacion ',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'item' => '25-Inconsistencia Claro Musica',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'item' => '26-Informa de Manera Errada Valor de Factura',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'item' => '27-No Genera Bloqueo Ofrecido',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'item' => '28-No Informa Prorrateos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'item' => '29-No Tramita Suspension Temporal',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'item' => '30-Inconsistecia Claro Video',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'item' => '31-No Informa Sobre Perdida de Descuento',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'item' => '32-No Escala Desactivacion de Paquete',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'item' => '33-Retenedio Sin Transfer',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'item' => '34-No Genera Bloqueo de SMS Ofrecido',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'item' => '35-No Indica Condiciones Completas Cambio de Ciclo',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'item' => '36-Radica Plan Par En La Linea Equivocada',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'item' => '37-Oferta Ajuste Sin Que El Titular Lo Solicitara',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'item' => '38-Cambio de Plan ',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'item' => '39-Informa de Manera Errada Fecha de Paso a Prepago',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'item' => '40-Cierra Desactiv/Plan Par Sin Que El Titular Lo Solicite',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'item' => '51-Otro',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'item' => 'Cerrado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 5,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'item' => 'Pendiente',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 5,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'item' => 'En Espera',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 5,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'item' => 'Procedimiento Correcto',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'item' => 'Sin Retenido',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'item' => 'No Tramita Cambio de Plan',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'item' => 'Linea Sin Transfer Con Gestion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'item' => 'Linea Sin Transfer Sin Gestion',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'item' => 'Lider No Envia Formato de Beneficio Ofrecido',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'item' => 'Linea Con Plan Par',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'item' => 'Otro',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'item' => 'Cerrado',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}