<?php

use Illuminate\Database\Seeder;

class RetenidosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('retenidos')->delete();
        
        \DB::table('retenidos')->insert(array (
            0 => 
            array (
                'idmaster' => 3,
                'backoffice_id' => 16769,
                'fecha' => '2019-10-10',
                'usuario_agente' => 12,
                'tipo_gestion' => 'INCONSISTENCIA',
                'contrato' => 458,
                'min_ac' => NULL,
                'ucid' => NULL,
                'custcode' => 'CODE123',
                'nombre_cliente' => 'JUAN DE LA OZ',
                'cedula_cliente' => 1044875485,
                'corte_linea' => 4451,
                'tipificacion' => 'NO TRAMITA CAMBIO DE PLAN',
                'tipificacion2' => 'NO SE HACE EL TRAMITE CORECTO A REFERIR',
                'tipificacion3' => 'GENERACION DE INCONSISTENCIAS AL PROCESAR ',
                'tipificacion4' => NULL,
                'motivo_cancelacion' => 'NO SE CANCELA',
                'observacion_cancelacion' => NULL,
                'solicita_ajuste' => 'NO',
                'valor_ajuste' => NULL,
                'call_transfiere' => NULL,
                'id_plan_actual' => NULL,
                'plan_actual' => NULL,
                'precio_actual' => NULL,
                'id_plan_nuevo' => 12,
                'plan_nuevo' => 'PLAN SUPER ELEGANTE',
                'precio_nuevo' => 125000,
                'valor_diferenciado' => 10000,
                'indicador_erosion' => '150',
                'fecha_corte' => '2019-10-15',
                'observacion1' => 'CORTE LOS DIAS 15 DE CADA MES',
                'observacion2' => 'CORTE LOS DIAS 15 DE CADA MES',
                'usuario_evasion' => 'HECTOR HUERTAS',
                'fecha_evasion' => '2019-10-10',
                'formato_novedades' => 'FORMATO #1',
                'tipificacion_erosion' => 'EROSION GESTION ACTIVA',
                'plan_actual_erosion' => 'SUPER PLAN GENIAL',
                'precio_actual_erosion' => 120000,
                'plan_nuevo_erosion' => 'PLAN NUEVO',
                'precio_nuevo_erosion' => 12000064,
               
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'idmaster' => 3,
                'backoffice_id' => 16769,
                'fecha' => '2019-10-10',
                'usuario_agente' => 12,
                'tipo_gestion' => 'INCONSISTENCIA',
                'contrato' => 459,
                'min_ac' => NULL,
                'ucid' => NULL,
                'custcode' => 'CODE456',
                'nombre_cliente' => 'LORD VALDOMERO',
                'cedula_cliente' => 5900458,
                'corte_linea' => 5562,
                'tipificacion' => 'LINEA INACTIVA',
                'tipificacion2' => 'LINEA NO CONCUERDA',
                'tipificacion3' => 'LINEA SIN TRANSFER',
                'tipificacion4' => NULL,
                'motivo_cancelacion' => 'NO SE CANCELA',
                'observacion_cancelacion' => NULL,
                'solicita_ajuste' => 'SI',
                'valor_ajuste' => '15000',
                'call_transfiere' => NULL,
                'id_plan_actual' => NULL,
                'plan_actual' => NULL,
                'precio_actual' => NULL,
                'id_plan_nuevo' => 13,
                'plan_nuevo' => 'PLAN SUPER GENIAL',
                'precio_nuevo' => 125000,
                'valor_diferenciado' => 10000,
                'indicador_erosion' => '150',
                'fecha_corte' => '2019-10-15',
                'observacion1' => 'CORTE LOS DIAS 15 DE CADA MES',
                'observacion2' => 'CORTE LOS DIAS 15 DE CADA MES',
                'usuario_evasion' => 'HECTOR HUERTAS',
                'fecha_evasion' => '2019-03-25',
                'formato_novedades' => 'FORMATO #15',
                'tipificacion_erosion' => 'EROSION GESTION ACTIVA',
                'plan_actual_erosion' => 'SUPER PLAN GENIAL',
                'precio_actual_erosion' => 120000,
                'plan_nuevo_erosion' => 'PLAN NUEVO',
                'precio_nuevo_erosion' => 12000066,
              
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}