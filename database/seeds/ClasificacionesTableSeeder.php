<?php

use Illuminate\Database\Seeder;

class ClasificacionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificaciones')->delete();
        
        \DB::table('clasificaciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Estado Cambio Plan',
                'descripcion' => 'Campos Para agregar los estados en backoffice cambio de plan',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Estado Evasiones',
                'descripcion' => 'Campos Para agregar los estados en backoffice evasiones',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Estado Cierre PQR',
                'descripcion' => 'Campos Para agregar los estados en backoffice cierre pqr',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Estado Inconsistencias',
                'descripcion' => 'Campos Para agregar los estados en backoffice inconsistencias',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Estado Casos Especiales',
                'descripcion' => 'Campos Para agregar los estados en backoffice casos especiales',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Estado Revision de Retenidos',
                'descripcion' => 'Campos Para agregar los estados en backoffice revision de retenidos',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Estado Correo',
                'descripcion' => 'Campos Para agregar los estados en backoffice correo',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Justificacion',
                'descripcion' => 'Campos para agregar las justificaciones de las inconsistencias',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Tipificacion',
                'descripcion' => 'Campos para agregar las tipificaciones deseadas',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}