<?php

use Illuminate\Database\Seeder;

class CierresPqrTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cierres_pqr')->delete();
        
        \DB::table('cierres_pqr')->insert(array (
            0 => 
            array (
                'backoffice_id' => 16769,
                'nr_cun' => NULL,
                'fecha_recibido' => '2019-10-10',
                'dias_sap' => NULL,
                'tipo_pqr' => 'QUEJA',
                'tipo_peticion' => 'RECURSO',
                'tipo_reclamo' => 'QUEJA Y RECURSO',
                'subreclamo' => NULL,
                'estado' => '1',
                'anexos' => 'NINGUNO',
                'remitente' => 'DESARROLLO',
                'radicaco_por' => 'DESARROLLO',
                'area_responsable' => 'DESARROLLO',
                'analista_responsable' => 'HECTOR HUERTAS',
                'min' => NULL,
                'fecha_silencio' => '2019-11-10',
                'consecutivo_salida' => 'Cons123456',
                'area_radicacion' => 'Centro',
                'codigo_id' => NULL,
                'region' => NULL,
                'direccion' => 'Calle Falsa 123',
                'ciudad' => 'Bogota',
                'distribuidor' => NULL,
                'ciclo_fact' => '70',
                'estado_id' => 70,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'backoffice_id' => 16769,
                'nr_cun' => NULL,
                'fecha_recibido' => '2019-10-10',
                'dias_sap' => NULL,
                'tipo_pqr' => 'RECURSO',
                'tipo_peticion' => 'QUEJA',
                'tipo_reclamo' => 'RECURSO Y QUEJA',
                'subreclamo' => NULL,
                'estado' => '1',
                'anexos' => 'NINGUNO',
                'remitente' => 'DESARROLLO',
                'radicaco_por' => 'DESARROLLO',
                'area_responsable' => 'DESARROLLO',
                'analista_responsable' => 'HECTOR HUERTAS',
                'min' => NULL,
                'fecha_silencio' => '2019-11-10',
                'consecutivo_salida' => 'Cons123456',
                'area_radicacion' => 'Centro',
                'codigo_id' => NULL,
                'region' => NULL,
                'direccion' => 'Calle Falsa 123',
                'ciudad' => 'Bogota',
                'distribuidor' => NULL,
                'ciclo_fact' => '70',
                'estado_id' => 70,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}