<?php

use Illuminate\Database\Seeder;

class InconsistenciasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('inconsistencias')->delete();
        
        \DB::table('inconsistencias')->insert(array (
            0 => 
            array (
                'backoffice_id' => 16769,
                'codigo_unico' => 123456,
                'fecha' => '2019-10-10',
                'custcode' => 'code123456',
                'tipo' => NULL,
                'inconsistencia' => 'absoluta',
                'cod_user' => '15',
                'nombre_consultor' => 'HECTOR HUERTAS',
                'ubicacion' => 'BOGOTA',
                'gerencia' => 'BOGOTA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'backoffice_id' => 16769,
                'codigo_unico' => 456789,
                'fecha' => '2019-10-10',
                'custcode' => 'code456789',
                'tipo' => NULL,
                'inconsistencia' => 'no tramita',
                'cod_user' => '16',
                'nombre_consultor' => 'HECTOR HUERTAS',
                'ubicacion' => 'BOGOTA',
                'gerencia' => 'BOGOTA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}