<?php

use Illuminate\Database\Seeder;

class GestionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gestiones')->delete();
        
        \DB::table('gestiones')->insert(array (
            0 => 
            array (
                'gestion_id' => NULL,
                'estado_id' => 1,
                'tipo_gestion' => 1,
                'asesor_id' => NULL,
                'observacion_asesor' => 'cambio de plan prueba1',
                'backoffice_id' => 16769,
                'observacion_backoffice' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'gestion_id' => NULL,
                'estado_id' => 2,
                'tipo_gestion' => 1,
                'asesor_id' => NULL,
                'observacion_asesor' => 'cambio de plan prueba2',
                'backoffice_id' => 16769,
                'observacion_backoffice' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'gestion_id' => NULL,
                'estado_id' => 4,
                'tipo_gestion' => 2,
                'asesor_id' => NULL,
                'observacion_asesor' => 'evasiones prueba1',
                'backoffice_id' => 16769,
                'observacion_backoffice' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'gestion_id' => NULL,
                'estado_id' => 5,
                'tipo_gestion' => 2,
                'asesor_id' => NULL,
                'observacion_asesor' => 'evasiones prueba2',
                'backoffice_id' => 16769,
                'observacion_backoffice' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}