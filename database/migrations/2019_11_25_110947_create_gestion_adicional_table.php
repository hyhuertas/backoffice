<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionAdicionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestion_adicional', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('min');
            $table->text('tipo_de_gestion');
            $table->integer('backoffice_id');
            $table->integer('tipo_gestion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestion_adicional');
    }
}
