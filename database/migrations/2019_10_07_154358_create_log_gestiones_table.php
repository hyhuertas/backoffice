<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogGestionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_gestiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gestion_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('rol_id')->nullable();
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->text('observacion')->nullable();
            $table->integer('tipo_gestion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_gestiones');
    }
}
