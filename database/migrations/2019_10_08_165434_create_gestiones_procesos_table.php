<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionesProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones_procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_inconsistencia');
            $table->date('fecha_reporte');
            $table->text('min');
            $table->text('custcode');
            $table->text('customer_id');
            $table->text('valor_sin_iva');
            $table->text('total');
            $table->text('rta_caso');
            $table->text('responsable');
            $table->text('cc');
            $table->text('user');
            $table->text('estado');
            $table->unsignedBigInteger('tipificacion_id');
            $table->text('motivo_otro')->nullable();
            $table->integer('backoffice_id')->nullable();
            $table->integer('gestion_id');
            $table->integer('tipo_gestion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestiones_procesos');
    }
}
