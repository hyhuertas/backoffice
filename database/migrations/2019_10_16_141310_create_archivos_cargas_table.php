<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivosCargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivos_cargas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero_registros_recibidos')->default(0);
            $table->integer('numero_registros_cargados')->default(0);
            $table->integer('numero_errores')->default(0);
            $table->integer('numero_fallas')->default(0);
            $table->string('ruta',200)->unique();
            $table->string('nombre_original',200);
            $table->string('nombre_archivo',200);
            $table->integer('tipo_gestion'); //3 para inconsistencias 4 para cierre pqr
            $table->string('id_usuario');
            $table->string('ruta_errores')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivos_cargas');
    }
}
