<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gestion_id')->nullable();
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->integer('tipo_gestion')->nullable(); //para saber si es de evasion o cambio plan
            $table->integer('asesor_id')->nullable();
            $table->text('observacion_asesor')->nullable();
            $table->integer('backoffice_id')->nullable();
            $table->text('observacion_backoffice')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestiones');
    }
}
