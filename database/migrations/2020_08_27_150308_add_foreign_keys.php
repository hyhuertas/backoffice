<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clasificacion_items', function($table) {
            $table->foreign('clasificacion_id')->references('id')->on('clasificaciones');
        });
        Schema::table('gestiones', function($table) {
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('log_gestiones', function($table) {
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('cierres_pqr', function($table) {
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('retenidos', function($table) {
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('casos_especiales', function($table) {
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('gestiones_procesos', function($table) {
            $table->foreign('tipificacion_id')->references('id')->on('clasificacion_items');
        });
        Schema::table('inconsistencias', function($table) {
            $table->foreign('justificacion_id')->references('id')->on('clasificacion_items');
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clasificacion_items', function($table) {
            $table->dropForeign('clasificacion_items_clasificacion_id_foreign');
        });
        Schema::table('gestiones', function($table) {
            $table->dropForeign('gestiones_estado_id_foreign');
        });
        Schema::table('log_gestiones', function($table) {
            $table->dropForeign('log_gestiones_estado_id_foreign');
        });
        Schema::table('cierres_pqr', function($table) {
            $table->dropForeign('cierres_pqr_estado_id_foreign');
        });
        Schema::table('retenidos', function($table) {
            $table->dropForeign('retenidos_estado_id_foreign');
        });
        Schema::table('casos_especiales', function($table) {
            $table->dropForeign('casos_especiales_estado_id_foreign');
        });
        Schema::table('gestiones_procesos', function($table) {
            $table->dropForeign('gestiones_procesos_tipificacion_id_foreign');
        });
        Schema::table('inconsistencias', function($table) {
            $table->dropForeign('inconsistencias_justificacion_id_foreign');
        });
        
    }
}
