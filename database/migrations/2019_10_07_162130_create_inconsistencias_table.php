<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInconsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inconsistencias', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->integer('backoffice_id')->nullable();
            $table->text('codigo_unico')->nullable();
            $table->date('fecha')->nullable();
            $table->text('custcode')->nullable();
            $table->text('tipo')->nullable();
            $table->text('inconsistencia')->nullable();
            $table->text('cod_user')->nullable();
            $table->text('nombre_consultor')->nullable();
            $table->text('ubicacion')->nullable();
            $table->text('gerencia')->nullable();
            $table->unsignedBigInteger('justificacion_id')->nullable();
            $table->enum('aplica_ajuste',['Si','No'])->nullable();
            $table->enum('ajuste_compartido',['Si','No'])->nullable();

            //aqui siguen los demas campos que hacen falta
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inconsistencias');
    }
}
