<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorreoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('backoffice_id');
            $table->text('min')->nullable();
            $table->text('custcode')->nullable();
            $table->text('numero_pqr')->nullable();
            $table->date('fecha_recibido')->nullable();
            $table->date('fecha_respuesta')->nullable();
            $table->text('remitente')->nullable();
            $table->text('respuesta')->nullable();
            $table->text('asunto')->nullable();
            $table->enum('aplica_ajuste', ['SI','NO']);
            $table->enum('ajuste_compartido', ['SI','NO'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correo');
    }
}
