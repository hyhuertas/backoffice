<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetenidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retenidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idmaster');
            $table->integer('backoffice_id')->nullable();
            $table->date('fecha')->nullable();
            $table->text('usuario_agente')->nullable();
            $table->text('tipo_gestion')->nullable();
            $table->text('contrato')->nullable();
            $table->text('min_ac')->nullable();
            $table->text('ucid')->nullable(); //campo de mas de 19 valores
            $table->text('custcode')->nullable();
            $table->text('nombre_cliente')->nullable();
            $table->text('cedula_cliente')->nullable();
            $table->integer('corte_linea')->nullable();
            $table->text('tipificacion')->nullable();
            $table->text('tipificacion2')->nullable();
            $table->text('tipificacion3')->nullable();
            $table->text('tipificacion4')->nullable();
            $table->text('motivo_cancelacion')->nullable();
            $table->text('observacion_cancelacion')->nullable();
            $table->text('solicita_ajuste')->nullable();
            $table->text('valor_ajuste')->nullable();
            $table->text('call_transfiere')->nullable();
            $table->integer('id_plan_actual')->nullable();
            $table->text('plan_actual')->nullable();
            $table->text('precio_actual')->nullable();
            $table->integer('id_plan_nuevo')->nullable();
            $table->text('plan_nuevo')->nullable();
            $table->text('precio_nuevo')->nullable();
            $table->text('valor_diferenciado')->nullable();
            $table->text('indicador_erosion')->nullable();
            $table->date('fecha_corte')->nullable();
            $table->text('observacion1')->nullable();
            $table->text('observacion2')->nullable();
            $table->text('usuario_evasion')->nullable();
            $table->date('fecha_evasion')->nullable();
            $table->text('formato_novedades')->nullable();
            $table->text('tipificacion_erosion')->nullable();
            $table->text('plan_actual_erosion')->nullable();
            $table->text('precio_actual_erosion')->nullable();
            $table->text('plan_nuevo_erosion')->nullable();
            $table->text('precio_nuevo_erosion')->nullable();
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->text('motivo_otro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retenidos');
    }
}
