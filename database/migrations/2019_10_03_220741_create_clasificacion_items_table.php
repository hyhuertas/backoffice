<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasificacionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificacion_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item',100);
            $table->integer('nivel');
            $table->integer('padre_id')->nullable();
            $table->unsignedBigInteger('clasificacion_id');
            $table->boolean('activo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clasificacion_items');
    }
}
