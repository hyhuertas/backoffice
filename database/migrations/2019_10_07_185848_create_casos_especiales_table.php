<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasosEspecialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos_especiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('backoffice_id');
            $table->text('numero_caso');
            $table->date('fecha_registro');
            $table->text('fecha_cierre')->nullable();
            $table->text('estado');
            $table->text('usuario_generador');
            $table->text('usuario_asignado');
            $table->text('observaciones');
            $table->text('respuesta');
            $table->text('nombre_cliente');
            $table->text('motivo_solicitud');
            $table->text('min');
            $table->date('fecha_comunicacion');
            $table->text('custcode');
            $table->text('user_ac');
            $table->text('descripcion_propuesta');
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->text('motivo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos_especiales');
    }
}
