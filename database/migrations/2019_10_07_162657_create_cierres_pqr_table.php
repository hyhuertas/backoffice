<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCierresPqrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cierres_pqr', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('backoffice_id')->nullable();
            $table->text('nr_cun')->nullable();
            $table->date('fecha_recibido')->nullable();
            $table->integer('dias_sap')->nullable();
            $table->text('tipo_pqr')->nullable();
            $table->text('tipo_peticion')->nullable();
            $table->text('tipo_reclamo')->nullable();
            $table->text('subreclamo')->nullable();
            $table->text('estado')->nullable();
            $table->text('anexos')->nullable();
            $table->text('remitente')->nullable();
            $table->text('radicaco_por')->nullable();
            $table->text('area_responsable')->nullable();
            $table->text('analista_responsable')->nullable();
            $table->text('min')->nullable();
            $table->date('fecha_silencio')->nullable();
            $table->text('consecutivo_salida')->nullable();
            $table->text('area_radicacion')->nullable();
            $table->text('codigo_id')->nullable(); //este campo es el id
            $table->text('region')->nullable();
            $table->text('direccion')->nullable();
            $table->text('ciudad')->nullable();
            $table->text('distribuidor')->nullable();
            $table->text('ciclo_fact')->nullable();
            $table->unsignedBigInteger('estado_id')->nullable(); //el estado propio en base de datos
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cierres_pqr');
    }
}
