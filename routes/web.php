<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppMasterController@appmaster')->name('home');
Route::get('/verGestiones', 'BackofficeController@verGestiones')->name('ver-gestiones');
Route::post('/listarGestion','BackofficeController@listarGestion')->name('listar-gestion');
Route::post('/tipificarGestion','BackofficeController@tipificarGestion')->name('tipificar-gestion');
Route::post('/listarClasificacionItems','ClasificacionItemsController@listarClasificacionItems')->name('listar-clasificacion-items');

Route::get('/verGestion/{opc}','GestionController@verGestionEstado')->name('ver-gestion-estado');
Route::post('/guardarGestionTablas','GestionController@guardarGestionTablas')->name('guardar-gestion-tablas'); //esta ruta sirve para guardar en las tablas de inconsistencias

Route::get('/cargarExcel','BackofficeController@cargarExcel')->name('cargar-excel');
Route::post('/cargarArchivoExcel','CargaExcelController@cargarArchivoExcel')->name('cargar-archivo-excel');
Route::get('/listarArchivoCarga','GestionController@listarArchivoCarga')->name('listar-archivo-carga');
Route::get('/listarArchivoCargaAdmin','AdministradorController@listarArchivoCargaAdmin')->name('listar-archivo-carga-admin');

Route::get('/cargarExcelAdmin','AdministradorController@cargarExcelAdmin')->name('cargar-excel-admin');
Route::post('/listarBackoffice','AdministradorController@listarBackoffice')->name('listar-backoffice');
Route::post('/cargarArchivoExcelAdmin','CargaExcelController@cargarArchivoExcelAdmin')->name('cargar-archivo-excel-admin');
Route::get('/correo', 'BackofficeController@correo')->name('tabla-correo');

Route::get('/usuarios', 'AdministradorController@adminUsuarios')->name('listar-usuarios');
Route::post('/listarRoles','AdministradorController@listarRoles')->name('listar-roles');
Route::post('/listarUsuarios', 'AdministradorController@listarUsuarios')->name('listar-usuarios-crm');

Route::post('/actualizarRol', 'AdministradorController@actualizarRol')->name('actualizar-rol');
Route::get('/descargarExcel', 'AdministradorController@reportes')->name('descargar-excel');
Route::post('/descargarInforme', 'AdministradorController@descargarExcelAdmin')->name('descargar-informe');

/* cargue adicional para el rol administrador */
Route::post('/cargarArchivoExcelAdminAdicional','CargaExcelController@cargarArchivoExcelAdminAdicional')->name('cargar-archivo-excel-admin-adicional');
Route::get('/cargarExcelAdminAdicional','AdministradorController@cargarExcelAdminAdicional')->name('cargar-excel-admin-adicional');
Route::get('/listarArchivoCargaAdminAdicional','AdministradorController@listarArchivoCargaAdminAdicionales')->name('listar-archivo-carga-admin-adicional');

/* cargue adicional para los backoffice */
Route::post('/cargarArchivoExcelAdicional','CargaExcelController@cargarArchivoExcelAdicional')->name('cargar-archivo-excel-adicional');
Route::get('/listarArchivoCargaAdicional','GestionController@listarArchivoCargaAdicional')->name('listar-archivo-carga-adicional');
Route::get('/cargarExcelAdicional','BackofficeController@cargarExcelAdicional')->name('cargar-excel-adicional');

/* descarga de excel adicional por parte del administrador */
Route::post('/descargarInformeAdicional', 'AdministradorController@descargarExcelAdminAdicional')->name('descargar-informe-adicional');
Route::get('/descargarExcelAdicional', 'AdministradorController@reportesAdicionales')->name('descargar-excel-adicional');


Route::post('/contarGestion','BackofficeController@contarGestion')->name('contar-gestion');
Route::post('/generarGrafica','BackofficeController@generarGrafica')->name('generar-grafica');

Route::get('/listarPlanes', 'AdministradorController@listarPlanes')->name('listarPlanes');

Route::get('/planes', 'AdministradorController@planes');
Route::post('/editarPlan', 'PlanesController@editarPlan')->name('editar-plan');
Route::post('/habilitarPlan', 'PlanesController@habilitarPlan')->name('habilitar-plan');
Route::post('/guardarPlan', 'PlanesController@guardarPlan')->name('guardar-plan');



