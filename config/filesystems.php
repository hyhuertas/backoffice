<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
                
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],
        'erroresInconsistencias' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Inconsistencias'),
             'url' => 'Errors/Inconsistencias',
            //'url' => env('APP_URL').'/storage/app/Errors/Inconsistencias', 
            'visibility' => 'public',
        ],
        'erroresRetenidos' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Retenidos'),
            'url' => 'Errors/Retenidos',
            // 'url' => env('APP_URL').'/storage/app/Errors/Retenidos',
            'visibility' => 'public',
        ],
        'erroresCierrePqr' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/CierrePQR'),
            'url' => 'Errors/CierrePQR',
            //'url' => env('APP_URL').'/storage/app/Errors/CierrePQR',
            'visibility' => 'public',
        ],
        'erroresCasosEspeciales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/CasosEspeciales'),
            'url' => 'Errors/CasosEspeciales',
            //'url' => env('APP_URL').'/storage/app/Errors/CasosEspeciales',
            'visibility' => 'public',
        ],
        'erroresInconsistenciasAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Inconsistencias/Adicionales'),
             'url' => 'Errors/Inconsistencias/Adicionales',
            //'url' => env('APP_URL').'/storage/app/Errors/Inconsistencias', 
            'visibility' => 'public',
        ],
        'erroresRetenidosAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Retenidos/Adicionales'),
            'url' => 'Errors/Retenidos/Adicionales',
            // 'url' => env('APP_URL').'/storage/app/Errors/Retenidos',
            'visibility' => 'public',
        ],
        'erroresCierrePqrAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/CierrePQR/Adicionales'),
            'url' => 'Errors/CierrePQR/Adicionales',
            //'url' => env('APP_URL').'/storage/app/Errors/CierrePQR',
            'visibility' => 'public',
        ],
        'erroresCasosEspecialesAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/CasosEspeciales/Adicionales'),
            'url' => 'Errors/CasosEspeciales/Adicionales',
            //'url' => env('APP_URL').'/storage/app/Errors/CasosEspeciales',
            'visibility' => 'public',
        ],
        'erroresCambioPlanAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/CambioPlan/Adicionales'),
            'url' => 'Errors/CambioPlan/Adicionales',
            // 'url' => env('APP_URL').'/storage/app/Errors/Retenidos',
            'visibility' => 'public',
        ],
        'erroresEvasionesAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Evasiones/Adicionales'),
            'url' => 'Errors/Evasiones/Adicionales',
            // 'url' => env('APP_URL').'/storage/app/Errors/Retenidos',
            'visibility' => 'public',
        ],
        'erroresCorreoAdicionales' => [
            'driver' => 'local',
            'root' => storage_path('app/Errors/Correo/Adicionales'),
            'url' => 'Errors/Correo/Adicionales',
            // 'url' => env('APP_URL').'/storage/app/Errors/Retenidos',
            'visibility' => 'public',
        ],

    ],

];
