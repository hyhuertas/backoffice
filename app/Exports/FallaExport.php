<?php

namespace App\Exports;

use App\Models\Errore;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class FallaExport implements   FromArray,WithTitle, WithMapping, WithHeadings, WithStrictNullComparison
{
    private $fallos;
    
    public function __construct($fallos) 
    {
        $this->fallos = $fallos;

    }
    
    
    
    public function title(): string
    {
        return 'Fallos';
    }
    public function array(): array
    {
        return $this->fallos;
    }
    
    public function map($fallo): array
    {
       
        return [
            $fallo['registro'],
            $fallo['columna'],
            $fallo['errors'],
            
        ];
    }
    
    public function headings(): array
    {
        return [
            'Registro',
            'Columna',
            'Errores',
        ];
    }
}