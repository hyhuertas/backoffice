<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CierrePqr extends Model 
{
   
    protected $table      = 'cierres_pqr';
    protected $primarykey = 'id';

    protected $fillable   = [
        'backoffice_id',
        'nr_cun',
        'fecha_recibido',
        'dias_sap',
        'tipo_pqr',
        'tipo_peticion',
        'tipo_reclamo',
        'subreclamo',
        'estado',
        'anexos',
        'remitente',
        'radicaco_por',
        'area_responsable',
        'analista_responsable',
        'min',
        'fecha_silencio',
        'consecutivo_salida',
        'area_radicacion',
        'codigo_id',
        'region',
        'direccion',
        'ciudad',
        'distribuidor',
        'ciclo_fact',
        'estado_id'
    ];

    public function tipificacion(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }

    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }

}
