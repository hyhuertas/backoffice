<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestionAnillo extends Model
{
    protected $connection = 'segundo_anillo_connection';
    protected $table = 'master';
}
