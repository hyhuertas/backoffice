<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Gestion extends Model 
{
    
    protected $table      = 'gestiones';
    protected $primarykey = 'id';

    protected $fillable   = ['gestion_id','estado_id','tipo_gestion','observacion_asesor','backoffice_id','observacion_backoffice'];

    public function estado(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }

    public function usuario_backoffice(){
        return $this->belongsTo('App\User', 'codigo_usercrm', 'backoffice_id');
    }
}
