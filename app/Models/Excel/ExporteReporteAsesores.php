<?php

namespace App\Models\Excel;

use App\Models\Gestion;
use App\Models\Inconsistencia;
use App\Models\CierrePqr;
use App\Models\CasoEspecial;
use App\Models\Retenido;
use App\Models\GestionProceso;
use App\Models\LogGestion;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Usuario;
Use App\Models\CambioPlan;
use DB;
use App\Models\Correo;
use App\Models\GestionAnillo;

class ExporteReporteAsesores implements FromView
{
    public function __construct(string $fechaInicio, string $fechaFinal, $rol_backoffice_id, $backoffice_id)
    {
       $this->fechaInicio=$fechaInicio;
       $this->fechaFinal=$fechaFinal;
       $this->rol_backoffice_id=$rol_backoffice_id;
       $this->backoffice_id=$backoffice_id;
       return $this->getData();
    }
    /* funcion para el exporte de la data a los distinton informes de backoffice */
    public function GetData()
    {

        $crm=$this->backoffice_id;
        $rol=$this->rol_backoffice_id;
        $desde=$this->fechaInicio.' 00:00:00';
        $hasta=$this->fechaFinal.' 23:59:59';

        /* si el rol es inconsistencias */
        if ($rol==4 && $crm!='') {
            $gestiones=Inconsistencia::where('backoffice_id', $crm)         
                     ->where('created_at', '>=', $desde)
                                     ->where('created_at', '<=', $hasta)
                                     ->orderBy('created_at','DESC')->get();                                     
        }elseif($rol==4){
            $gestiones=Inconsistencia::where('created_at', '>=', $desde)
                                     ->where('created_at', '<=', $hasta)
                                     ->orderBy('created_at','DESC')->get();
        }
        /* si rol es cierre pqr */
        if ($rol==5 && $crm!='') {
            $gestiones=CierrePqr::where('backoffice_id', $crm) 
                                ->where('created_at', '>=', $desde)
                                ->where('created_at', '<=', $hasta)
                                ->orderBy('created_at','DESC')->get();            
            
        }elseif ($rol==5) {
            $gestiones=CierrePqr::where('created_at', '>=', $desde)
                                ->where('created_at', '<=', $hasta)
                                ->orderBy('created_at','DESC')->get();            
        }
        /* si rol es caso especial */
        if($rol==6 && $crm!='') {
            $gestiones=CasoEspecial::where('backoffice_id', $crm) 
                                   ->where('created_at', '>=', $desde)
                                   ->where('created_at', '<=', $hasta)
                                   ->orderBy('created_at','DESC')->get();
        }elseif ($rol==6) {
            $gestiones=CasoEspecial::where('created_at', '>=', $desde)
                                   ->where('created_at', '<=', $hasta)
                                   ->orderBy('created_at','DESC')->get();
        }
        /* si el rol es retenidos */
        if($rol==7 && $crm!='') {
            $gestiones=Retenido::where('backoffice_id', $crm)
                               ->where('created_at', '>=', $desde)
                               ->where('created_at', '<=', $hasta)
                               ->orderBy('created_at','DESC')->get();
        }elseif ($rol==7) {
            $gestiones=Retenido::where('created_at', '>=', $desde)
                               ->where('created_at', '<=', $hasta)
                               ->orderBy('created_at','DESC')->get();        
        }
        /* si el rol es cambio de plan */
        /* prueba foreach cambio plan */
        if($rol==2 && $crm==''){
            $gestiones=Gestion::where('created_at', '>=', $desde)
                              ->where('created_at', '<=', $hasta)
                              ->where('tipo_gestion', 1)
                              ->orderBy('created_at','DESC')->get();                
                  
            foreach ($gestiones as $gestion) {

                $gestion->cambio_plan=CambioPlan::where('idcambio_plan',$gestion->gestion_id)->orderBy('idcambio_plan')->first();                               
                $gestion->usuarioAsesor=Usuario::where('id_usuario',$gestion->asesor_id)->first(); 
                $gestion->usuarioBackoffice=User::where('codigo_usercrm',$gestion->backoffice_id)->first();                
                               
            }               
        }elseif ($rol==2 && $crm!='') {
            $gestiones=Gestion::where('backoffice_id', $crm)
                              ->where('created_at', '>=', $desde)
                              ->where('created_at', '<=', $hasta)
                              ->where('tipo_gestion', 1)
                              ->orderBy('created_at','DESC')->get();                
                  
            foreach ($gestiones as $gestion) {

                $gestion->cambio_plan=CambioPlan::where('idcambio_plan',$gestion->gestion_id)->orderBy('idcambio_plan')->first();                               
                $gestion->usuarioAsesor=Usuario::where('id_usuario',$gestion->asesor_id)->first(); 
                $gestion->usuarioBackoffice=User::where('codigo_usercrm',$gestion->backoffice_id)->first();                
                                               
            }  
        }
        /* si el rol es Correo */
        if ($rol==8) {
            $gestiones=Correo::where('created_at', '>=', $desde)
                             ->where('created_at', '<=', $hasta)
                             ->orderBy('created_at','DESC')->get();

        }elseif($rol==8 && $crm!=''){
            $gestiones=Correo::where('backoffice_id', $crm)
                              ->where('created_at', '>=', $desde)
                              ->where('created_at', '<=', $hasta)
                              ->orderBy('created_at','DESC')->get();
        }
        /* si el rol es evasiones */
        if ($rol==3 && $crm=='') {
            $gestiones=Gestion::where('created_at', '>=', $desde)
                              ->where('created_at', '<=', $hasta)
                              ->where('tipo_gestion', 2)
                              ->orderBy('created_at','DESC')->get();                
                  
            foreach ($gestiones as $gestion) {

                $gestion->evasiones=GestionAnillo::where('idmaster',$gestion->gestion_id)->orderBy('idmaster')->first();                               
                $gestion->usuarioAsesor=Usuario::where('id_usuario',$gestion->asesor_id)->first(); 
                $gestion->usuarioBackoffice=User::where('codigo_usercrm',$gestion->backoffice_id)->first();                
                               
            }
        }elseif ($rol==3 && $crm!='') {
            $gestiones=Gestion::where('backoffice_id', $crm)
                              ->where('created_at', '>=', $desde)
                              ->where('created_at', '<=', $hasta)
                              ->where('tipo_gestion', 2)
                              ->orderBy('created_at','DESC')->get();                
                  
            foreach ($gestiones as $gestion) {

                $gestion->evasiones=GestionAnillo::where('idmaster',$gestion->gestion_id)->orderBy('idmaster')->first();                               
                $gestion->usuarioAsesor=Usuario::where('id_usuario',$gestion->asesor_id)->first(); 
                $gestion->usuarioBackoffice=User::where('codigo_usercrm',$gestion->backoffice_id)->first();                
                               
            }
        }
        return $gestiones;
        } 
        


    public function view(): View
    {
        $crm=$this->backoffice_id;
        $rol=$this->rol_backoffice_id;
        if($rol==4) {
            return view('reportes.excel-reporte-inconsistencias', [
                'gestiones' => $this->getData()
                ]);
        }     
        if($rol==5) {
            return view('reportes.excel-reporte-cierre-pqr', [
                'gestiones' => $this->getData()
                ]);
        }
        if($rol==6) {
            return view('reportes.excel-reporte-caso-especial', [
                'gestiones' => $this->getData()
                ]);
        }
        if($rol==7) {
            return view('reportes.excel-reporte-retenido', [
                'gestiones' => $this->getData()
                ]);
        }
        if($rol==2) {
            return view('reportes.excel-reporte-cambio-plan', [
                'gestiones' => $this->getData()
                ]);
        }
        if ($rol==8) {
            return view('reportes.excel-reporte-correo', [
                'gestiones' => $this->getData()
                ]);       
        }
        if ($rol==3) {
            return view('reportes.excel-reporte-evasiones', [
                'gestiones' => $this->getData()
            ]);
        }
    }
}
