<?php

namespace App\Models\Excel;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Usuario;
use DB;
use App\Models\GestionAnillo;

use App\Models\GestionAdicional;

class ExporteReporteAdicional implements FromView
{
    public function __construct(string $fechaInicio, string $fechaFinal, $rol_backoffice_id, $backoffice_id)
    {
       $this->fechaInicio=$fechaInicio;
       $this->fechaFinal=$fechaFinal;
       $this->rol_backoffice_id=$rol_backoffice_id;
       $this->backoffice_id=$backoffice_id;
       return $this->getData();
    }
    /* funcion para el exporte de la data a los distinton informes de backoffice */
    public function getData()
    {

        DB::beginTransaction();
        try{

            $crm=$this->backoffice_id;
            $rol=$this->rol_backoffice_id;
            $desde=$this->fechaInicio.' 00:00:00';
            $hasta=$this->fechaFinal.' 23:59:59';
            $rol_administrador=auth()->user()->rol_user_id;
    
            // $gestiones=GestionAdicional::query();
            // $gestiones
            $gestiones=GestionAdicional::where('created_at', '>=', $desde)
            ->where('created_at', '<=', $hasta);
            
            
            if($crm!=''){
                $gestiones=$gestiones->where('backoffice_id',$crm);
            }
            
    
            /* si el rol es inconsistencias */
            if ($rol==4) { $tipo_gestion=3; }
            /* si rol es cierre pqr */
            if ($rol==5) { $tipo_gestion=4; }
            /* si rol es caso especial */
            if ($rol==6) { $tipo_gestion=5; }
            /* si el rol es retenidos */
            if ($rol==7) { $tipo_gestion=6; }
            /* si el rol es cambio de plan */
            if ($rol==2) { $tipo_gestion=1; }
            /* si el rol es Correo */
            if ($rol==8) { $tipo_gestion=9; }
            /* si el rol es evasiones */
            if ($rol==3) { $tipo_gestion=2; }
           
            $gestiones=$gestiones->where('tipo_gestion', $tipo_gestion);
          
            $gestiones=$gestiones->orderBy('created_at','DESC')->get();   
            
           
            
            return $gestiones;
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
       
    }

    public function view(): View
    {        
        return view('reportes.excel-reporte-adicional', [
            'gestiones' => $this->getData()
            ]);
    }
}
