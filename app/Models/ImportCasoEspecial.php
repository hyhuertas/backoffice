<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CasoEspecial;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;

class ImportCasoEspecial implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id;

    public function __construct($backoffice_id)
    {
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }else{
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }
        //$this->backoffice_id=auth()->user()->codigo_usercrm;
    }
    public function model(array $row)
    {
        $this->rows++;//para contabilizar el total de inserciones
        
        return new CasoEspecial([
            'numero_caso'      => intval(doubleval($row['numero_caso'])),
            'fecha_registro'     => date("Y-m-d",strtotime($row['fecha_registro'])),
            'fecha_cierre'    =>date("Y-m-d",strtotime($row['fecha_cierre'])), 
            'estado'    =>$row['estado'], 
            'usuario_generador'    =>$row['usuario_generador'], 
            'usuario_asignado'    =>$row['usuario_asignado'], 
            'observaciones'    =>$row['observaciones'], 
            'respuesta'      =>$row['respuesta'],
            'nombre_cliente'     =>$row['nombre_cliente'],
            'motivo_solicitud'      =>$row['motivo_solicitud'],
            'min'=>strval($row['min']),
            'fecha_comunicacion'=>date("Y-m-d",strtotime($row['fecha_comunicacion'])),
            'custcode'    =>strval($row['custcode']),
            'user_ac'=>$row['user_ac'],
            'descripcion_propuesta'=>$row['descripcion_propuesta'],
            'backoffice_id'=>$this->backoffice_id,
        ]);

    }
    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        
        return [
/*             'numero_caso'    => 'nullable|numeric|unique:casos_especiales,numero_caso', 
            'fecha_registro'    => 'nullable|date',
            'fecha_cierre'    => 'nullable|date',
            'estado'    => 'nullable|string|max:255',
            'usuario_generador'      => 'nullable|string|max:255',
            'usuario_asignado'     => 'nullable|string|max:255',
            'observaciones' =>'nullable|string|max:255',
            'respuesta' =>'nullable|string|max:255',
            'nombre_cliente' => 'nullable|string|max:255',
            'motivo_solicitud' => 'nullable|string|max:255',
            'min'=>'nullable|integer',
            'fecha_comunicacion'=>'nullable|date',
            'custcode'    => 'nullable|integer',
            'user_ac'=>'nullable|string|max:255',
            'descripcion_propuesta'=>'nullable|string|max:255', */

        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          $this->fallos++;//para contabilizar el totl de fallos
          foreach ($failures as $failure) {
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row();
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {
         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();
        
      }
}
