<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GestionAdicional;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;


class ImportGestionAdicional implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id='';
    public $tipo_Gestion='';
    public $rol='';
   
    public function __construct($backoffice_id, $tipo_Gestion, $rol)
    {
        
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }elseif($backoffice_id==''){
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }

        if($this->backoffice_id == auth()->user()->codigo_usercrm){
            $this->rol=auth()->user()->rol_user_id;
        }else{
            $this->rol = $rol;
        }

  //ASIGNACION PARA LOS DIFERENTES ROLES Y SUS TIPOS DE GESTION
        if ($this->rol==4) {//inconsistencias
            $this->tipo_Gestion=3;
        }elseif ($this->rol==5) {//cierre pqr
            $this->tipo_Gestion=4;
        }elseif ($this->rol==6) {//casos especiales
            $this->tipo_Gestion=5;
        }elseif ($this->rol==7) {//retenidos
            $this->tipo_Gestion=6;
        }elseif ($this->rol==1) {//administrador
            $this->tipo_Gestion=7;
        }elseif ($this->rol==2) {//cambio plan
            $this->tipo_Gestion=1;
        }elseif($this->rol==3) {//Evasiones
            $this->tipo_Gestion=2;
        }elseif($this->rol==8) {//correo
            $this->tipo_Gestion=9;
        }
        
        //$this->backoffice_id=auth()->user()->codigo_usercrm;
    }
    public function model(array $row)
    {   
        $this->rows++;//para contabilizar el total de inserciones
        return new GestionAdicional([
            'min'      => strval($row['min']),
            'tipo_de_gestion'       => strval($row['tipo_de_gestion']),
            'backoffice_id'     => $this->backoffice_id,
            'tipo_gestion'      => $this->tipo_Gestion
        ]);

    }
    /**
     * Se indica que los datos comienzan desde la fila 2
     */

    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        
        return [
    /*         'min'    => 'nullable|string|max:255|unique:gestion_adicional,min',
            'tipo_de_gestion'      => 'nullable|string|max:255',
            'backoffice_id'     => 'nullable|string|max:255',
            'tipo_gestion'    => 'nullable|integer',           */ 
        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          $this->fallos++;//para contabilizar el totl de fallos
          foreach ($failures as $failure) {
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row();
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {
         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();
        
      }
}
