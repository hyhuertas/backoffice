<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $timestamps = false;
    protected $connection = 'master_connection';
    protected $table = 'usuarios';
    protected $fillable = ['id_usuario','nombre_usuario', 'apellido_usuario', 'usuario', 'password','estado','tipoUsuario'];
}
