<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Clasificacion extends Model 
{
   
    
    protected $table      = 'clasificaciones';
    protected $primarykey = 'id';

    protected $fillable   = ['nombre','descripcion'];
    
    public function clasificacionItems(){
        return $this->hasMany('App\Models\ClasificacionItems','clasificacion_id','id');
    }

}
