<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Retenido extends Model 
{
    
    protected $table      = 'retenidos';
    protected $primarykey = 'id';

    protected $fillable   = [
        'idmaster',
        'backoffice_id',
        'fecha',
        'usuario_agente',
        'tipo_gestion',
        'contrato',
        'min_ac',
        'ucid',
        'custcode',
        'nombre_cliente',
        'cedula_cliente',
        'corte_linea',
        'tipificacion',
        'tipificacion2',
        'tipificacion3',
        'tipificacion4',
        'motivo_cancelacion',
        'observacion_cancelacion',
        'solicita_ajuste',
        'valor_ajuste',
        'call_transfiere',
        'id_plan_actual',
        'plan_actual',
        'precio_actual',
        'id_plan_nuevo',
        'plan_nuevo',
        'precio_nuevo',
        'valor_diferenciado',
        'indicador_erosion',
        'fecha_corte',
        'observacion1',
        'observacion2',
        'usuario_evasion',
        'fecha_evasion',
        'formato_novedades',
        'tipificacion_erosion',
        'plan_actual_erosion',
        'precio_actual_erosion',
        'plan_nuevo_erosion',
        'precio_nuevo_erosion',
        'estado_id'
    ];

    public function estado(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }
    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }
}
