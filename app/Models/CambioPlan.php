<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CambioPlan extends Model
{
    protected $connection = 'segundo_anillo_connection';
    protected $table = 'cambio_plan';
    protected $fillable=['idcambio_plan'];
    
}
