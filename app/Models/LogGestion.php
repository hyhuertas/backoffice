<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LogGestion extends Model 
{
    
    protected $table      = 'log_gestiones';
    protected $primarykey = 'id';

    protected $fillable   = ['gestion_id','user_id','rol_id','estado_id','observacion','tipo_gestion'];
    
    public function estado(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }
}
