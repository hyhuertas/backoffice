<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
    protected $table      = 'correo';
    protected $primarykey = 'id';

    protected $fillable   = [
        'backoffice_id',
        'min',
        'custcode',
        'numero_pqr',
        'aplica_ajuste',
        'ajuste_compartido',
                
    ];

    public function estado(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }

    public function gestionProceso(){
        return $this->hasMany('App\Models\GestionProceso','gestion_id','id');
    }

    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }
}
