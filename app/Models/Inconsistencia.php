<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Inconsistencia extends Model 
{
    
    protected $table      = 'inconsistencias';
    protected $primarykey = 'id';

        protected $fillable   = [
        'backoffice_id',
        'codigo_unico',
        'fecha',
        'custcode',
        'tipo',
        'inconsistencia',
        'cod_user',
        'nombre_consultor',
        'ubicacion',
        'gerencia',
        'justificacion_id',
        'aplica_ajuste'
    ];
    public function estado(){
        return $this->belongsTo('App\Models\ClasificacionItems','justificacion_id','id');
    }
    public function gestionProceso(){
        return $this->hasMany('App\Models\GestionProceso','gestion_id','id');
    }
}
