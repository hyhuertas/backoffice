<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Retenido;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;
use Validator;


class ImportRetenido implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id;
   
    public function __construct($backoffice_id)
    {
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }else{
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }
        //$this->backoffice_id=auth()->user()->codigo_usercrm;
    }
    public function model(array $row)
    {

        $this->rows++;//para contabilizar el total de inserciones
        return new Retenido([
            'idmaster'      => $row['idmaster'],
            'fecha'     => date("Y-m-d",strtotime($row['fecha_creacion'])),
            'usuario_agente'    =>intval(doubleval($row['usuario_agente'])), 
            'tipo_gestion'    =>$row['tipo_gestion'], 
            'contrato'    =>intval(doubleval($row['contrato'])), 
            'min_ac'    =>intval(doubleval($row['min_ac'])), 
            'ucid'      =>intval(doubleval($row['ucid'])),
            'custcode'     =>intval(doubleval($row['custcode'])),
            'nombre_cliente'     =>$row['nombre_cliente'],
            'cedula_cliente'     =>intval(doubleval($row['cedula_cliente'])),
            'corte_linea'     =>intval(doubleval($row['corte_linea'])),
            'tipificacion'     =>$row['tipificacion'],
            'tipificacion2'     =>$row['tipificacion2'],
            'tipificacion3'     =>$row['tipificacion3'],
            'tipificacion4'     =>$row['tipificacion4'],
            'motivo_cancelacion'     =>$row['motivo_de_cancelacion'],
            'observacion_cancelacion'     =>$row['observaciones_de_motivo_de_cancelacion'],
            'solicita_ajuste'     =>$row['solicita_ajuste'],
            'valor_ajuste'     =>$row['valor_ajuste'],
            'call_transfiere'     =>$row['call_transfiere'],
            'id_plan_actual'     =>$row['id_plan_actual'],
            'plan_actual'     =>$row['plan_actual'],
            'precio_actual'     =>$row['precio_actual'],
            'id_plan_nuevo'     =>$row['id_plan_nuevo'],
            'plan_nuevo'     =>$row['plan_nuevo'],
            'precio_nuevo'     =>$row['precio_nuevo'],
            'valor_diferenciado'     =>$row['valor_diferenciado'],
            'indicador_erosion'     =>$row['indicador_erosion'],
            'fecha_corte'     =>date("Y-m-d",strtotime($row['fecha_corte'])),
            'observacion1'     =>$row['observacion1'],
            'observacion2'     =>$row['observacion2'],
            'usuario_evasion'     =>$row['usuario_que_genera_la_evasion'],
            'fecha_evasion'     =>date("Y-m-d",strtotime($row['fecha_evasion'])),
            'formato_novedades'     =>$row['formato_de_novedades_call_center'],
            'tipificacion_erosion'     =>$row['tipificacion_erosion'],
            'plan_actual_erosion'     =>$row['plan_actual_erosion'],
            'precio_actual_erosion'     =>$row['precio_actual_erosion'],
            'Plan_nuevo_erosion'     =>$row['plan_nuevo_erosion'],
            'Precio_nuevo_erosion'     =>$row['precio_nuevo_erosion'],
            'backoffice_id'=>$this->backoffice_id,
        ]);

    }
    /**
     * Se indica que los datos comienzan desde la fila 2
     */

    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        
        return [
/*             'idmaster'    => 'nullable|integer|unique:retenidos,idmaster', 
            'fecha_creacion'    => 'nullable|date',
            'usuario_agente'    => 'nullable|integer',
            'tipo_gestion'    => 'nullable|string|max:255',
            'contrato'      => 'nullable|numeric',
            'min_ac'     => 'nullable|numeric',
            'ucid' =>'nullable|numeric',
            'custcode' =>'nullable|numeric',
            'nombre_cliente' => 'nullable|string|max:255',
            'cedula_cliente' => 'nullable|numeric',
            'corte_linea' => 'nullable|numeric',
            'tipificacion' => 'nullable|string|max:255',
            'tipificacion2' => 'nullable|string|max:255',
            'tipificacion3' => 'nullable|string|max:255',
            'tipificacion4' => 'nullable|string|max:255',
            'motivo de cancelacion' => 'nullable|string|max:255',
            'observaciones de motivo de cancelacion' => 'nullable|string|max:255',
            'solicita_ajuste' => 'nullable|string|max:255',
            'valor_ajuste' => 'nullable|string|max:255',
            'call_transfiere' => 'nullable|string|max:255',
            'id_plan_actual' => 'nullable|integer',
            'plan_actual' => 'nullable|string|max:255',
            'precio_actual' => 'nullable|numeric',
            'id_plan_nuevo' => 'nullable|integer',
            'plan_nuevo' => 'nullable|string|max:255',
            'precio_nuevo' => 'nullable|numeric',
            'valor_diferenciado' => 'nullable|numeric',
            'indicador_erosion' => 'nullable|string|max:255',
            'fecha_corte' => 'nullable|date',
            'observacion1' => 'nullable|string|max:255',
            'observacion2' => 'nullable|string|max:255',
            'Usuario que genera la evasion' => 'nullable|string|max:255',
            'Fecha Evasión' => 'nullable|string|max:255',
            'Formato de Novedades Call Center' => 'nullable|string|max:255',
            'Tipificacion Erosion' => 'nullable|string|max:255',
            'Plan actual erosion' => 'nullable|string|max:255',
            'Precio actual erosión' => 'nullable|string|max:255',
            'Plan nuevo erosión' => 'nullable|string|max:255',
            'Precio nuevo erosión' => 'nullable|string|max:255', */
           
        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          $this->fallos++;//para contabilizar el totl de fallos
          foreach ($failures as $failure) {
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row();
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {
         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();
        
      }
}
