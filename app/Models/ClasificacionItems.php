<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClasificacionItems extends Model 
{
   
    protected $table      = 'clasificacion_items';
    protected $primarykey = 'id';
    protected $fillable   = ['item','nivel','padre_id','clasificacion_id','activo'];
    
    public function clasificacion(){
        return $this->belongsTo('App\Models\Clasificacion','clasificacion_id','id');
    }
}
