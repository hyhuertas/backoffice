<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $connection = 'segundo_anillo_connection';
    protected $table = 'planes';
    protected $primaryKey = 'idplanes';
    protected $fillable=['idplanes', 'plan', 'precio', 'tipo', 'habilitado', 'precio_sin_iva'];
    public  $timestamps = false;
}
