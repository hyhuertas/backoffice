<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CasoEspecial extends Model 
{
    
    protected $table      = 'casos_especiales';
    protected $primarykey = 'id';

    protected $fillable   = [
        'backoffice_id',
        'numero_caso',
        'fecha_registro',
        'fecha_cierre',
        'estado',
        'usuario_generador',
        'usuario_asignado',
        'observaciones',
        'respuesta',
        'nombre_cliente',
        'motivo_solicitud',
        'min',
        'fecha_comunicacion',
        'custcode',
        'user_ac',
        'descripcion_propuesta',
        'estado_id',
        'motivo'
    ];
    public function tipificacion(){
        return $this->belongsTo('App\Models\ClasificacionItems','estado_id','id');
    }
    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }
}
