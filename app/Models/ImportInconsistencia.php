<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inconsistencia;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;


class ImportInconsistencia implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id='';
   
    public function __construct($backoffice_id)
    {
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }elseif($backoffice_id==''){
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }
        //$this->backoffice_id=auth()->user()->codigo_usercrm;
    }
    public function model(array $row)
    {   
        $this->rows++;//para contabilizar el total de inserciones
        
        return new Inconsistencia([
            'codigo_unico'      => strval($row['codigo_unico']),
            'fecha'     => date("Y-m-d",strtotime($row['fecha'])),
            'custcode'    =>strval($row['custcode']), 
            'tipo'    =>$row['tipo'], 
            'inconsistencia'    =>$row['inconsistencia'], 
            'cod_user'    =>$row['cod_user'], 
            'nombre_consultor'    =>$row['nombre_consultor'], 
            'ubicacion'      =>$row['ubicacion'],
            'gerencia'     =>$row['gerencia'],
            'backoffice_id'=>$this->backoffice_id,
        ]);

    }
    /**
     * Se indica que los datos comienzan desde la fila 2
     */

    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        
        return [
/*             'codigo_unico'    => 'nullable|integer|unique:inconsistencias,codigo_unico', 
            'fecha'    => 'nullable|date',
            'custcode'    => 'nullable|integer',
            'tipo'    => 'nullable|string|max:255',
            'inconsistencia'      => 'nullable|string|max:255',
            'cod_user'     => 'nullable|string|max:255',
            'nombre_consultor' =>'nullable|string|max:255',
            'ubicacion' =>'nullable|string|max:255',
            'gerencia' => 'nullable|string|max:255', */
           
        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          $this->fallos++;//para contabilizar el totl de fallos
          foreach ($failures as $failure) {
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row();
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {
         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();
        
      }
}
