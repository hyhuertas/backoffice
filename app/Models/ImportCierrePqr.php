<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CierrePqr;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;

class ImportCierrePqr implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id;
   
    public function __construct($backoffice_id)
    {
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }else{
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }
        //$this->backoffice_id=auth()->user()->codigo_usercrm;
    }
    public function model(array $row)
    {
        $this->rows++;//para contabilizar el total de inserciones

        return new CierrePqr([
            'nr_cun'      => intval(doubleval($row['nrcun'])),
            'fecha_recibido'     => date("Y-m-d",strtotime($row['fecha_recibido'])),
            'dias_sap'    =>strval($row['dias_sap']), 
            'tipo_pqr'    =>$row['tipo_pqr'], 
            'tipo_peticion'    =>$row['tipo_peticion'], 
            'tipo_reclamo'    =>$row['tipo_reclamo'], 
            'subreclamo'    =>$row['subreclamo'], 
            'estado'      =>$row['estado'],
            'anexos'     =>$row['anexos'],
            'remitente'=>$row['remitente'],
            'radicado_por'=>$row['radicado_por'],
            'area_responsable'=>$row['area_responsable'],
            'analista_responsable'=>$row['analista_responsable'],
            'min'=>strval($row['min']),
            'fecha_silencio'=>date("Y-m-d",strtotime($row['fecha_silencio'])),
            'consecutivo_salida'=>$row['consecutivo_salida'],
            'area_radicacion'=>$row['area_radicacion'],
            'codigo_id'=>strval($row['id']),
            'region'=>$row['region'],
            'direccion'=>$row['direccion'],
            'ciudad'=>$row['ciudad'],
            'distribuidor'=>$row['distribuidor'],
            'ciclo_fact'=>$row['ciclo_fact'],
            'backoffice_id'=>$this->backoffice_id,
        ]);

    }
    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        
        return [
/*             'nrcun'    => 'nullable|numeric|unique:cierres_pqr,nr_cun', 
            'fecha_recibido'    => 'nullable|date',
            'dias_sap'    => 'nullable|integer',
            'tipo_pqr'    => 'nullable|string|max:255',
            'tipo_peticion'      => 'nullable|string|max:255',
            'tipo_reclamo'     => 'nullable|string|max:255',
            'subreclamo' =>'nullable|string|max:255',
            'estado' =>'nullable|string|max:255',
            'anexos' => 'nullable',
            'remitente'=>'nullable|string|max:255',
            'radicado_por'=>'nullable|string|max:255',
            'area_responsable'=>'nullable|string|max:255',
            'analista_responsable'=>'nullable|string|max:255',
            'min'=>'nullable|integer',
            'fecha_silencio'=>'nullable|date',
            'consecutivo_salida'=>'nullable|string|max:255',
            'area_radicacion'=>'nullable|string|max:255',
            'id'=>'integer|unique:cierres_pqr,codigo_id',
            'region'=>'nullable|string|max:255',
            'direccion'=>'nullable|string|max:255',
            'ciudad'=>'nullable|string|max:255',
            'distribuidor'=>'nullable|string|max:255',
            'ciclo_fact'=>'nullable|string|max:255', */


           
        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          $this->fallos++;//para contabilizar el totl de fallos
          foreach ($failures as $failure) {
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row();
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {
         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();
        
      }
}
