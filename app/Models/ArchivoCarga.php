<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArchivoCarga extends Model
{
    protected $table="archivos_cargas";
    protected $fillable=['numero_registros_recibidos','numero_registros_cargados','ruta','nombre_original','nombre_archivo','tipo_gestion','ruta_errores', 'id_usuario'];
}
