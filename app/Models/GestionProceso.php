<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GestionProceso extends Model 
{
    
    protected $table      = 'gestiones_procesos';
    protected $primarykey = 'id';

    protected $fillable   = [
        'fecha_inconsistencia',
        'fecha_reporte',
        'min',
        'custcode',
        'customer_id',
        'valor_sin_iva',
        'total',
        'rta_caso',
        'responsable',
        'user',
        'estado',
        'tipificacion_id',
        'motivo_otro',
        'backoffice_id',
        'gestion_id',
        'tipo_gestion'
    ];

    public function tipificacion(){
        return $this->belongsTo('App\Models\ClasificacionItems','tipificacion_id','id');
    }
    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }
}
