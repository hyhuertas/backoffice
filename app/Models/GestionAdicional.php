<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestionAdicional extends Model
{
    protected $table="gestion_adicional";
    protected $fillable=['min','tipo_de_gestion','backoffice_id','tipo_gestion'];

    public function usuario(){
        return $this->belongsTo('App\User','backoffice_id','codigo_usercrm');
    }
}
