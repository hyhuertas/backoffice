<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EditarPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        
        //exists:segundo_anillo_connection.planes|unique:planes,plan,$idplanes,idplanes
        return [
            'model.plan'=>sprintf('required|string|max:255|unique:segundo_anillo_connection.planes,plan,%d,idplanes',Request::instance()->model['idplan']),
            'model.precio'=>'required|integer|min:500',
        ];
    }
    public function attributes()
    {
        return [
            'model.plan' => 'Plan',
            'model.precio'=>'Precio'
        ]; 
    }
}
