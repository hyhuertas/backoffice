<?php

namespace App\Http\Controllers;

use App\Exports\CargueExport;
use App\Models\ArchivoCarga;
use App\Models\ImportInconsistencia;
use App\Models\ImportRetenido;
use App\Models\ImportCierrePqr;
use App\Models\ImportCasoEspecial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Models\GestionAdicional;
use App\Models\ImportGestionAdicional;
use App\Models\ArchivoCargaAdicional;


class CargaExcelController extends Controller
{
    public function cargarArchivoExcel(Request $request){
        DB::beginTransaction();
       
        try {
            $rol=auth()->user()->rol_user_id;
            $backoffice_id=auth()->user()->codigo_usercrm;
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');
            $ext = $file->getClientOriginalExtension();//extension del archivo
            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['file'=>'El formato y extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }
            $store="";
            $modelo="";
            $diskErrors="";
            //si el rol es de inconsistencias
            if($rol==4){

                $modelo=new ImportInconsistencia($backoffice_id);
                $store='Imports/Inconsistencias';
                $diskErrors="erroresInconsistencias";
                $tipoGestion=3;
            /* si el rol es cierre pqr */
            }elseif($rol==5){

                $modelo=new ImportCierrePqr($backoffice_id);
                $store='Imports/CierrePQR';
                $diskErrors="erroresCierrePqr";
                $tipoGestion=4;
            /* si el rol es caso especial */
            }elseif ($rol==6) {

                $modelo=new ImportCasoEspecial($backoffice_id);
                $store='Imports/CasosEspeciales';
                $diskErrors="erroresCasosEspeciales";
                $tipoGestion=5;
            }
            /* si el rol es retenidos */
            elseif($rol==7){

                $modelo=new ImportRetenido($backoffice_id);
                $store='Imports/Retenidos';
                $diskErrors="erroresRetenidos";
                $tipoGestion=6;
                                
            }elseif($rol==2){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CambioPlan/Adicionales';
                $diskErrors="erroresCambioPlanAdicionales";
                $tipoGestion=1; //para cambio de plan                                
            }elseif($rol==3){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Evasiones/Adicionales';
                $diskErrors="erroresEvasionesAdicionales";
                $tipoGestion=2; //para evasiones                               
            }
            elseif($rol==8){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Correo/Adicionales';
                $diskErrors="erroresCorreoAdicionales";
                $tipoGestion=9;//para correo                                
            }
            $import=$modelo; //se instancia la clase de importacion
            Excel::import( $import,$file);
            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);
            $archivoCarga=new ArchivoCarga();
            $archivoCarga->nombre_original = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo =  $random_name;
            $archivoCarga->ruta =  $path;
            $archivoCarga->id_usuario = $backoffice_id;
            $rows=$import->rows;
            $total=0;

            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);

                $pathErrors=Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->numero_fallas=$import->fallos;
            }
            $archivoCarga->tipo_gestion= $tipoGestion;
            
            $archivoCarga->numero_registros_cargados=$rows;
            $archivoCarga->save(); 
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);

        }
    }
    public function cargarArchivoExcelAdicional(Request $request){
        DB::beginTransaction();
       
        try {
            $rol=auth()->user()->rol_user_id;
            $backoffice_id=auth()->user()->codigo_usercrm;
            $tipo_Gestion='';
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');
           
            $ext = $file->getClientOriginalExtension();//extension del archivo
            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['file'=>'El formato y extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }
            $store="";
            $modelo="";
            $diskErrors="";
            //si el rol es de inconsistencias
            if($rol==4){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Inconsistencias/Adicionales';
                $diskErrors="erroresInconsistenciasAdicionales";
                $tipoGestion=3; //para inconsistencias
            /* si el rol es cierre pqr */
            }elseif($rol==5){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CierrePQR/Adicionales';
                $diskErrors="erroresCierrePqrAdicionales";
                $tipoGestion=4; //para cierre pqr
            /* si el rol es caso especial */
            }elseif ($rol==6) {

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CasosEspeciales/Adicionales';
                $diskErrors="erroresCasosEspecialesAdicionales";
                $tipoGestion=5; //para casos especiales
            }elseif($rol==7){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Retenidos/Adicionales';
                $diskErrors="erroresRetenidosAdicionales";
                $tipoGestion=6;  //para retenidos                              
            }elseif($rol==2){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CambioPlan/Adicionales';
                $diskErrors="erroresCambioPlanAdicionales";
                $tipoGestion=1; //para cambio de plan                                
            }elseif($rol==3){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Evasiones/Adicionales';
                $diskErrors="erroresEvasionesAdicionales";
                $tipoGestion=2; //para evasiones                               
            }
            elseif($rol==8){

                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Correo/Adicionales';
                $diskErrors="erroresCorreoAdicionales";
                $tipoGestion=9;//para correo                                
            }
            $import=$modelo; //se instancia la clase de importacion
            Excel::import( $import,$file);
            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);
            $archivoCarga=new ArchivoCargaAdicional();
            $archivoCarga->nombre_original = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo =  $random_name;
            $archivoCarga->ruta =  $path;
            $archivoCarga->id_usuario = $backoffice_id;
            $rows=$import->rows;
            $total=0;

            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);

                $pathErrors=Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->numero_fallas=$import->fallos;
            }
            $archivoCarga->tipo_gestion= $tipoGestion;
            
            $archivoCarga->numero_registros_cargados=$rows;
            $archivoCarga->save(); 
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);

        }
    }
    public function generateRandomString($length = 8) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    public function cargarArchivoExcelAdmin(Request $request){

        //DB::beginTransaction();
        try {
            $rol=$request->rol_backoffice_id;
            $backoffice_id=$request->backoffice_id;
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');
            //dd($rol,$backoffice_id); //dejo aqui para la carga de excel del admin
            $ext=$file->getClientOriginalExtension();//extension del archivo

            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['file'=>'El formato y extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }
            $store="";
            $modelo="";
            $diskErrors="";
            //si el rol es de inconsistencias
            if($rol==4){

                $modelo=new ImportInconsistencia($backoffice_id);
                $store='Imports/Inconsistencias';
                $diskErrors="erroresInconsistencias";
                $tipoGestion=7;
            /* si el rol es cierre pqr */
            }elseif($rol==5){

                $modelo=new ImportCierrePqr($backoffice_id);
                $store='Imports/CierrePQR';
                $diskErrors="erroresCierrePqr";
                $tipoGestion=7;
            /* si el rol es caso especial */
            }elseif ($rol==6) {

                $modelo=new ImportCasoEspecial($backoffice_id);
                $store='Imports/CasosEspeciales';
                $diskErrors="erroresCasosEspeciales";
                $tipoGestion=7;
            }
            /* si el rol es retenidos */
            if($rol==7){

                $modelo=new ImportRetenido($backoffice_id);
                $store='Imports/Retenidos';
                $diskErrors="erroresRetenidos";
                $tipoGestion=7;
                                
            }
            $import=$modelo; //se instancia la clase de importacion
            Excel::import( $import,$file);
            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);
            $archivoCarga=new ArchivoCarga();
            $archivoCarga->nombre_original = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo =  $random_name;
            $archivoCarga->id_usuario = $backoffice_id;
            $archivoCarga->ruta =  $path;
            $rows=$import->rows;
            $total=0;
            
            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);

                $pathErrors=Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->numero_fallas=$import->fallos;
            }
            $archivoCarga->tipo_gestion= $tipoGestion;
            
            $archivoCarga->numero_registros_cargados=$rows;
            $archivoCarga->save(); 
            
            DB::commit();
        } catch (\Exception $e) {
            
            DB::rollback();
            dd($e);
        }
        
    }
    /* funcion para cargue de archivo adicional al rol administrador */
    public function cargarArchivoExcelAdminAdicional(Request $request){

        DB::beginTransaction();
        try {
            $rol=$request->rol_backoffice_id;
            $backoffice_id=$request->backoffice_id;
            $tipo_Gestion='';
         
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');
            //dd($rol,$backoffice_id); //dejo aqui para la carga de excel del admin
            $ext=$file->getClientOriginalExtension();//extension del archivo

            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['file'=>'El formato y extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }
            $store="";
            $modelo="";
            $diskErrors="";
            //si el rol es de inconsistencias
            if($rol==4){

                $tipoGestion=7;
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Inconsistencias/Adicionales';
                $diskErrors="erroresInconsistenciasAdicionales";
            /* si el rol es cierre pqr */
            }elseif($rol==5){

                $tipoGestion=7;
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CierrePQR/Adicionales';
                $diskErrors="erroresCierrePqrAdicionales";
            /* si el rol es caso especial */
            }elseif ($rol==6) {

                $tipoGestion=7;
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CasosEspeciales/Adicionales';
                $diskErrors="erroresCasosEspecialesAdicionales";
            /* si el rol es retenidos */
            }elseif($rol==7){

                $tipoGestion=7;
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Retenidos/Adicionales';
                $diskErrors="erroresRetenidosAdicionales";
                                
            }elseif($rol==2){

                $tipoGestion=7;
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/CambioPlan/Adicionales';
                $diskErrors="erroresCambioPlanAdicionales";
            }elseif($rol==3){

                $tipoGestion=7;                               
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Evasiones/Adicionales';
                $diskErrors="erroresEvasionesAdicionales";
            }
            elseif($rol==8){

                $tipoGestion=7;                              
                $modelo=new ImportGestionAdicional($backoffice_id, $tipo_Gestion, $rol);
                $store='Imports/Correo/Adicionales';
                $diskErrors="erroresCorreoAdicionales";
            }
            $import=$modelo; //se instancia la clase de importacion
            Excel::import( $import,$file);
            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);
            $archivoCarga=new ArchivoCargaAdicional();
            $archivoCarga->nombre_original = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo = $random_name;
            $archivoCarga->ruta =  $path;
            $archivoCarga->id_usuario = $backoffice_id;
            $rows=$import->rows;
            $total=0;
            
            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);

                $pathErrors=Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->numero_fallas=$import->fallos;
            }
            $archivoCarga->tipo_gestion= $tipoGestion;
            
            $archivoCarga->numero_registros_cargados=$rows;
            $archivoCarga->save(); 
            
            DB::commit();
        } catch (\Exception $e) {
            
            DB::rollback();
            dd($e);
        }
        
    } 
}