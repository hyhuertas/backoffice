<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\RolBackoffice;
use App\Models\Usuario;
use App\Models\Excel\ExporteReporteAsesores;
use App\Models\Excel\ExporteReporteAdicional;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ArchivoCarga;
use App\Models\ArchivoCargaAdicional;
use DB;
use App\Models\Plan;
class AdministradorController extends Controller
{
    public function cargarExcelAdmin(){
        return view('administrador.admin-cargar-excel');
    }

    public function cargarExcelAdminAdicional(){
        return view('administrador.admin-cargar-excel-adicional');
    }

    public function listarBackoffice(Request $request){
        $backoffice_id=$request->rol_backoffice_id;
        if(isset($backoffice_id)){
            $backoffice=User::where('rol_user_id',$backoffice_id)->get();
        }
        return $backoffice;
    }
    public function adminUsuarios()
    {
        return view('administrador.admin-listar-usuarios');
    }

    public function listarUsuarios(){
        $usuarios=Usuario::join('roles','roles.id_usuario','usuarios.id_usuario')
          ->where('roles.id_modulo','=',session('crm'))
          ->where('estado', '=', 'Habilitado')
          ->select(DB::raw("concat(nombre_usuario,' ',apellido_usuario) as user"),'roles.id_usuario','roles.numero_rol')
          ->get();
        foreach ($usuarios as $usuario) {
            $usuario->descripcion_rol=RolBackoffice::where('idrol', $usuario->numero_rol)->first();
        }

        return $usuarios;
    }
    public function actualizarRol(Request $request){
        DB::beginTransaction();
       
        try{
            $actualizarRol=Rol::where('id_usuario',$request->idusu)
                            ->where('id_modulo',session('crm'))
                            ->update(['numero_rol'=>$request->rol_backoffice_id]);
                            
            $user=User::where('codigo_usercrm',$request->idusu)->first();
            $user->rol_user_id=$request->rol_backoffice_id;
            $user->save();              
            
        DB::commit();   
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function listarRoles(Request $request){
        $UsuariosCrm=RolBackoffice::where('rol', '!=', 9)->get();

        return $UsuariosCrm;
    } 
    public function reportes(){

        return view('administrador.admin-descargar-excel');
    }
    public function reportesAdicionales(){

        return view('administrador.admin-descargar-excel-adicional');
    }

    public function descargarExcelAdmin(Request $request){

        $fechaInicial=$request->fechaInicial;
        $fechaFinal=$request->fechaFinal;
        $rol_backoffice_id=$request->rol_backoffice_id;
        $backoffice_id=$request->backoffice_id;

        $fecha=date('d-m-Y');
        $reporte=new ExporteReporteAsesores($fechaInicial,  $fechaFinal, $rol_backoffice_id, $backoffice_id);
        return Excel::download($reporte, 'Reporte_general_'.$fecha.'.xlsx');
    }
    public function descargarExcelAdminAdicional(Request $request){

        $fechaInicial=$request->fechaInicial;
        $fechaFinal=$request->fechaFinal;
        $rol_backoffice_id=$request->rol_backoffice_id;
        $backoffice_id=$request->backoffice_id;
        $fecha=date('d-m-Y');
        $reporte=new ExporteReporteAdicional($fechaInicial,  $fechaFinal, $rol_backoffice_id, $backoffice_id);
        return Excel::download($reporte, 'Reporte_general_'.$fecha.'.xlsx');
    }
    public function listarArchivoCargaAdmin(){
        //$rol=auth()->user()->rol_user_id;

           $archivoCarga=ArchivoCarga::where('tipo_gestion',7)
           ->orderBy('created_at','DESC')
           ->get();

        return $archivoCarga;       
    }
    public function listarArchivoCargaAdminAdicionales(){
        //$rol=auth()->user()->rol_user_id;

           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',7)
           ->orderBy('created_at','DESC')
           ->get();

        return $archivoCargaAdicional;       
    }

    public function planes(Request $request){
        
        if(isset($request->estado)){    
            $planes = Plan::where('habilitado',$request->estado)->get();
        }else{
            $planes = Plan::all();
        }
       
        return $planes;
    }

    public function listarPlanes(){

        return view('administrador.admin-planes');
    }

    
}
