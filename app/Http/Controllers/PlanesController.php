<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditarPlanRequest;
use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlanesController extends Controller
{
    
    public function editarPlan(EditarPlanRequest $request){
        $model=(object)$request->model;
        $plan=Plan::find($model->idplan);
        $plan->plan=$model->plan;
        $plan->precio=$model->precio;
        $plan->save();
        
    }
    public function habilitarPlan(Request $request){
        $opc=$request->opc;
        
        $plan_id=$request->plan_id;
        $plan=Plan::find($plan_id);
        $plan->habilitado=$opc;
        $plan->save();
    }
    public function guardarPlan(EditarPlanRequest $request){
        $model=(object)$request->model;
        $plan=new Plan();
        $plan->plan=$model->plan;
        $plan->precio=$model->precio;
        $plan->habilitado=1;
        $plan->save();
    }
}
