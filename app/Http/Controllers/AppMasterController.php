<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Rol;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class AppMasterController extends Controller
{
    public function appmaster()
    {
        $crm= $_GET['crm'];
 
        try {
             //$usuario_id= Crypt::decrypt($_GET['id_usuario']);
            $usuario_id=$_GET['idusu'];

            if(isset($usuario_id)){
                $rol = Rol::where('id_usuario', '=', $usuario_id)
                ->where('id_modulo', '=',$crm)
                ->first();
    
                $usu = Usuario::where('id_usuario', '=', $usuario_id)
                ->where('estado', '=', 'Habilitado')
                ->where('tipoUsuario', '=', 'Interno')
                ->first();
 
                $user=User::where('codigo_usercrm',$rol->id_usuario )->first();
                if(!isset($user)){//si no existe el usuario
                        $user= new User();//se define una nueva instancia
                        $user->password=Hash::make(123456);
                }
                
                //se actualiza o se crea el nuevo usuario
                $user->nombre=$usu->nombre_usuario.' '.$usu->apellido_usuario;
                $user->numero_documento=$usu->cedula_usuario;
                $user->codigo_usercrm=$usu->id_usuario;
                $user->rol_user_id=$rol->numero_rol;
                $user->save();
    
                Auth::login($user, true);
                $rolusu = $rol->numero_rol;
                $idusu = $usuario_id;
                $crm=$_GET['crm'] ?? session('crm');
                //se crean variable de sesion para redirigir a la vista de usuario
                session(['rolusu'=> $rolusu]);
                session(['idusu'=> $idusu]);
                session(['crm'=> $crm]);
    
                if($rolusu=="1"){
                    return view('administrador.admin-home',compact('idusu','crm'));
    
                }
    
               if ($rolusu=="5") {
                    return view('backofficeCierrePqr.backoffice-cierre-pqr-home', compact('idusu', 'crm'));
               }
    
                if($rolusu=="2" || $rolusu=="3" || $rolusu=="4" || $rolusu=="6" || $rolusu=="7" || $rolusu=="8"){
                    
                    return view('backoffice.backoffice-home',compact('idusu','crm'));
               }
               
    
            }else{
                abort('404');
            }         
        } catch (DecryptException $e) {
           
            abort('404');
        }            
        }
    }
