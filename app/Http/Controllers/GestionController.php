<?php

namespace App\Http\Controllers;

use App\Models\ArchivoCarga;
use App\Models\ClasificacionItems;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\LogGestion;
use App\Models\Usuario;
use App\Models\CambioPlan;
use App\Models\CierrePqr;
use App\Models\GestionAnillo;
use App\Models\GestionProceso;
use App\Models\Inconsistencia;
use App\Models\RolBackoffice;
use App\Models\CasoEspecial;
use App\Models\Retenido;
use App\Models\Correo;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\ArchivoCargaAdicional;

class GestionController extends Controller
{
    //function que me retorna la vista la data de la gestion dependiendo del estado en el que se encuentre
    public function verGestionEstado($opc){
        $rol=auth()->user()->rol_user_id;
        $codigoCrm=auth()->user()->codigo_usercrm;

        
        //validacion para backoffice cambio de plan
        if($opc==1 && $rol==2 || $opc==2 && $rol==2 || $opc==3 && $rol==2){
            $gestiones=Gestion::with('estado')
        ->where('estado_id',$opc)
        ->where('backoffice_id', $codigoCrm);
        
            $gestiones=$gestiones->where('tipo_gestion',1)
            ->orderBy('created_at','DESC')->get();
            
            
            foreach ($gestiones as $gestion) {
                
                $gestion->asesor=Usuario::where('id_usuario',$gestion->asesor_id)->first();
                $gestion->gestionAnillo=CambioPlan::where('idcambio_plan',$gestion->gestion_id)->first();
                $logGestiones=LogGestion::with('estado')->where('gestion_id',$gestion->id)
                ->where('tipo_gestion',1)->get();
                foreach ($logGestiones as  $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario',$logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol',$logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;
            }
            
            
            
        }elseif($opc==4 && $rol==3 || $opc==5 && $rol==3){
            $gestiones=Gestion::with('estado')
            ->where('estado_id',$opc)
            ->where('backoffice_id', $codigoCrm);
            $gestiones=$gestiones->where('tipo_gestion',2)
            ->orderBy('created_at','DESC')->get();

            foreach ($gestiones as $gestion) {
                
                $gestion->asesor=Usuario::where('id_usuario',$gestion->asesor_id)->first();
                $gestion->gestionAnillo=GestionAnillo::where('idmaster',$gestion->gestion_id)->first();
                $logGestiones=LogGestion::with('estado')->where('gestion_id',$gestion->id)
                ->where('tipo_gestion',2)->get();
                foreach ($logGestiones as  $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario',$logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol',$logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;

            }
            //si el rol es inconsistencias se muestran todos los campos de inconsistencias
        }elseif($opc==6 && $rol==4){
            $gestiones=Inconsistencia::where('backoffice_id',$codigoCrm)
            ->where('justificacion_id','!=',null)->get();
            foreach ($gestiones as $gestion) {
                $gestion->gestionProceso=GestionProceso::with('tipificacion')->where('gestion_id',$gestion->id)->get();
                $logGestiones=LogGestion::with('estado')->where('gestion_id',$gestion->id)
                ->where('tipo_gestion',3)->get();
                foreach ($logGestiones as  $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario',$logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol',$logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;
            }
        }elseif ($opc==7 && $rol==5) {
            $gestiones=CierrePqr::where('backoffice_id', $codigoCrm)
            ->where('estado_id','!=', null)->get();
            foreach ($gestiones as $gestion) {
                $logGestiones=LogGestion::with('estado')->where('gestion_id', $gestion->id)
                ->where('tipo_gestion',4)->get();
                foreach ($logGestiones as $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario', $logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol', $logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;
            }
        }elseif ($opc==8 && $rol==6) {
            $gestiones=CasoEspecial::where('backoffice_id', $codigoCrm)
            ->where('estado_id','!=', null)->get();
            foreach ($gestiones as $gestion) {
                $logGestiones=LogGestion::with('estado')->where('gestion_id', $gestion->id)
                ->where('tipo_gestion',5)->get();
                foreach ($logGestiones as $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario', $logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol', $logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;
            }
        }elseif ($opc==9 && $rol==7) {
            $gestiones=Retenido::where('backoffice_id', $codigoCrm)
            ->where('estado_id','!=', null)->get();
            foreach ($gestiones as $gestion) {
                $logGestiones=LogGestion::with('estado')->where('gestion_id', $gestion->id)
                ->where('tipo_gestion',6)->get();
                foreach ($logGestiones as $logGestion) {
                    $logGestion->usuario=Usuario::where('id_usuario', $logGestion->user_id)->first();
                    $logGestion->rol=RolBackoffice::where('idrol', $logGestion->rol_id)->pluck('descripcion')->first();
                }
                $gestion->logGestion=$logGestiones;
            }
        }else{
            abort('403');
        }
        return view('backoffice.backoffice-ver-gestion-estado',compact(['gestiones','opc']));
    }    
     public function listarArchivoCarga(){
         $rol=auth()->user()->rol_user_id;
         $id_usuario=auth()->user()->codigo_usercrm;
         if($rol==4){
            $archivoCarga=ArchivoCarga::where('tipo_gestion',3)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
         }elseif($rol==5){
            $archivoCarga=ArchivoCarga::where('tipo_gestion',4)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
         }elseif($rol==6){
            $archivoCarga=ArchivoCarga::where('tipo_gestion',5)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
         }elseif($rol==7){
            $archivoCarga=ArchivoCarga::where('tipo_gestion',6)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
         }         
         return $archivoCarga;
        
     }
     public function listarArchivoCargaAdicional(){
        $rol=auth()->user()->rol_user_id;
        $id_usuario=auth()->user()->codigo_usercrm;
        if($rol==4){
           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',3)
           ->where('id_usuario', $id_usuario)
           ->orderBy('created_at','DESC')
           ->get();
        }elseif($rol==5){
           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',4)
           ->where('id_usuario', $id_usuario)
           ->orderBy('created_at','DESC')
           ->get();
        }elseif($rol==6){
           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',5)
           ->where('id_usuario', $id_usuario)
           ->orderBy('created_at','DESC')
           ->get();
        }elseif($rol==7){
           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',6)
           ->where('id_usuario', $id_usuario)
           ->orderBy('created_at','DESC')
           ->get();
        }elseif($rol==1){
           $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',7)
           ->where('id_usuario', $id_usuario)
           ->orderBy('created_at','DESC')
           ->get();
        }elseif($rol==2){
            $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',1)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
        }
        elseif($rol==3){
            $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',2)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
        }
        elseif($rol==8){
            $archivoCargaAdicional=ArchivoCargaAdicional::where('tipo_gestion',9)
            ->where('id_usuario', $id_usuario)
            ->orderBy('created_at','DESC')
            ->get();
        }

         return $archivoCargaAdicional;
       
    }
     

    //funcion para guardar en las tablas de inconsistencias,cierre PQR, Retenidos y casos especiales
    public function guardarGestionTablas(Request $request){
        $rol=auth()->user()->rol_user_id;
        $backoffice_id=auth()->user()->codigo_usercrm;
        //si el rol es inconsistencias
        DB::beginTransaction();
        try{
            if($rol==4){
                $form=(object)$request->model;
                
                $gestion_id=$request->gestion_id;
                $gestion=Inconsistencia::find($gestion_id);
                $gestion->justificacion_id=$form->justificacion_id;
                $gestion->aplica_ajuste=$form->aplica_ajuste;
                $gestion->ajuste_compartido=$form->ajuste_compartido;
                $gestion->save();
                if($gestion->id && $gestion->aplica_ajuste=='Si' ){
                    
                    $gestionProceso1=new GestionProceso();
                    $gestionProceso1->fecha_inconsistencia=$form->fecha_inconsistencia;
                    $gestionProceso1->fecha_reporte=$form->fecha_reporte;
                    $gestionProceso1->min=$form->min;
                    $gestionProceso1->custcode=$form->custcode;
                    $gestionProceso1->customer_id=$form->customer_id;
                    $gestionProceso1->valor_sin_iva=$form->valor_sin_iva;
                    $gestionProceso1->total=$form->total;
                    $gestionProceso1->rta_caso=$form->rta_caso;
                    $gestionProceso1->responsable=$form->responsable;
                    $gestionProceso1->cc=$form->cc;
                    $gestionProceso1->user=$form->user;
                    $gestionProceso1->estado=$form->estado;
                    $gestionProceso1->tipificacion_id=$form->tipificacion_id;
                    if(!empty($form->otro)){
                        $gestionProceso1->motivo_otro=$form->otro;
                    }
                    $gestionProceso1->backoffice_id=$backoffice_id;
                    $gestionProceso1->gestion_id=$gestion->id;
                    $gestionProceso1->tipo_gestion=3;//el tipo de gestion para inconsistencias es 3
                    $gestionProceso1->save(); 
                    if($gestion->ajuste_compartido=='Si'){
                        $gestionProceso2=new GestionProceso(); //guarda el segundo proceso
                        $gestionProceso2->fecha_inconsistencia=$form->fecha_inconsistencia2;
                        $gestionProceso2->fecha_reporte=$form->fecha_reporte2;
                        $gestionProceso2->min=$form->min2;
                        $gestionProceso2->custcode=$form->custcode2;
                        $gestionProceso2->customer_id=$form->customer_id2;
                        $gestionProceso2->valor_sin_iva=$form->valor_sin_iva2;
                        $gestionProceso2->total=$form->total2;
                        $gestionProceso2->rta_caso=$form->rta_caso2;
                        $gestionProceso2->responsable=$form->responsable2;
                        $gestionProceso2->cc=$form->cc2;
                        $gestionProceso2->user=$form->user2;
                        $gestionProceso2->estado=$form->estado2;
                        $gestionProceso2->tipificacion_id=$form->tipificacion_id2;
                        if(!empty($form->otro2)){
                            $gestionProceso2->motivo_otro=$form->otro2;
                        }
                        $gestionProceso2->backoffice_id=$backoffice_id;
                        $gestionProceso2->gestion_id=$gestion->id;
                        $gestionProceso2->tipo_gestion=3;//el tipo de gestion para inconsistencias es 3
                        $gestionProceso2->save();
                    }
                    
                }
                $logGestion= new LogGestion();
                    $logGestion->gestion_id=$gestion->id;
                    $logGestion->user_id=$backoffice_id;
                    $logGestion->rol_id=$rol;
                    $logGestion->estado_id=$gestion->justificacion_id;
                    $logGestion->tipo_gestion=3; //el 3 para inconsistencias
                    $logGestion->save();
      
            }
            /* si el rol es cierre pqr */
            if ($rol==5) {
                $form=(object) $request->model;
                $gestion_id=$request->gestion_id;
                $gestion=CierrePqr::find($gestion_id);
                $gestion->estado_id=$form->tipificacion_id;
                $gestion->save();
                if($gestion->id){
                    
                    $logGestion= new LogGestion();
                    $logGestion->gestion_id=$gestion->id;
                    $logGestion->user_id=$backoffice_id;
                    $logGestion->rol_id=$rol;
                    $logGestion->estado_id=$form->tipificacion_id;
                    $logGestion->tipo_gestion=4; //el 4 para Cierre PQR
                    $logGestion->save();
                }
            }
            if ($rol==6) {
                $form=(object) $request->model;
                $gestion_id=$request->gestion_id;
                $gestion=CasoEspecial::find($gestion_id);
                $gestion->estado_id=$form->tipificacion_id;
                if($form->tipificacion_id==60 && !empty($form->pendiente)){
                    $gestion->motivo=$form->pendiente;
                }
                if($form->tipificacion_id==61 && !empty($form->en_espera)){
                    $gestion->motivo=$form->en_espera;
                }
                $gestion->save();
                if($gestion->id){
                    
                    $logGestion= new LogGestion();
                    $logGestion->gestion_id=$gestion->id;
                    $logGestion->user_id=$backoffice_id;
                    $logGestion->rol_id=$rol;
                    $logGestion->estado_id=$form->tipificacion_id;
                    $logGestion->tipo_gestion=5; //el 5 para Casos Especiales
                    if($form->tipificacion_id==60 && !empty($form->pendiente)){
                        $logGestion->observacion=$form->pendiente;
                    }
                    if($form->tipificacion_id==61 && !empty($form->en_espera)){
                        $logGestion->observacion=$form->en_espera;
                    }
                    $logGestion->save();
                }
            }
            if ($rol==7) {
                $form=(object) $request->model;
                $gestion_id=$request->gestion_id;
                $gestion=Retenido::find($gestion_id);
                $gestion->estado_id=$form->tipificacion_id;               
                if($form->tipificacion_id==69 && !empty($form->otro2)){
                    $gestion->motivo_otro=$form->otro2;
                }
                $gestion->save();
                if($gestion->id){
                    
                    $logGestion= new LogGestion();
                    $logGestion->gestion_id=$gestion->id;
                    $logGestion->user_id=$backoffice_id;
                    $logGestion->rol_id=$rol;
                    $logGestion->estado_id=$form->tipificacion_id;
                    $logGestion->tipo_gestion=6; //el 6 para Retenido
                    if(!empty($form->otro2)){
                        $logGestion->observacion=$form->otro2;
                    }
                    $logGestion->save();
                }
            }
            if($rol==8){

                $form=(object) $request->model;
                $gestion_id=$request->gestion_id;
                $gestion=Correo::find($gestion_id);
                $gestion= new Correo(); 
                $gestion->backoffice_id=$backoffice_id;
                $gestion->min=$form->min;
                $gestion->custcode=$form->custcode;
                $gestion->numero_pqr=$form->numero_pqr;
                $gestion->fecha_recibido=$form->fecha_recibido;
                $gestion->fecha_respuesta=$form->fecha_respuesta;
                $gestion->remitente=$form->remitente;
                $gestion->respuesta=$form->respuesta;
                $gestion->asunto=$form->asunto;
                $gestion->aplica_ajuste=$form->aplica_ajuste;
                $gestion->ajuste_compartido=$form->ajuste_compartido;    
                $gestion->save();         
                if($gestion->id && $gestion->aplica_ajuste=='Si' ){    

                    $gestionProceso1=new GestionProceso();
                    $gestionProceso1->fecha_inconsistencia=$form->fecha_inconsistencia;
                    $gestionProceso1->fecha_reporte=$form->fecha_reporte;
                    $gestionProceso1->min=$form->min2;
                    $gestionProceso1->custcode=$form->custcode2;
                    $gestionProceso1->customer_id=$form->customer_id;
                    $gestionProceso1->valor_sin_iva=$form->valor_sin_iva;
                    $gestionProceso1->total=$form->total;
                    $gestionProceso1->rta_caso=$form->rta_caso;
                    $gestionProceso1->responsable=$form->responsable;
                    $gestionProceso1->cc=$form->cc;
                    $gestionProceso1->user=$form->user;
                    $gestionProceso1->estado=$form->estado;
                    $gestionProceso1->tipificacion_id=$form->tipificacion_id;
                    if(!empty($form->otro)){
                        $gestionProceso1->motivo_otro=$form->otro;
                    }
                    $gestionProceso1->backoffice_id=$backoffice_id;
                    $gestionProceso1->gestion_id=$gestion->id;
                    $gestionProceso1->tipo_gestion=7;//el tipo de gestion para correo es 7
                    $gestionProceso1->save(); 
               

                    if($gestion->ajuste_compartido=='Si'){
                        $gestionProceso2=new GestionProceso(); //guarda el segundo proceso
                        $gestionProceso2->fecha_inconsistencia=$form->fecha_inconsistencia2;
                        $gestionProceso2->fecha_reporte=$form->fecha_reporte2;
                        $gestionProceso2->min=$form->min3;
                        $gestionProceso2->custcode=$form->custcode3;
                        $gestionProceso2->customer_id=$form->customer_id2;
                        $gestionProceso2->valor_sin_iva=$form->valor_sin_iva2;
                        $gestionProceso2->total=$form->total2;
                        $gestionProceso2->rta_caso=$form->rta_caso2;
                        $gestionProceso2->responsable=$form->responsable2;
                        $gestionProceso2->cc=$form->cc2;
                        $gestionProceso2->user=$form->user2;
                        $gestionProceso2->estado=$form->estado2;
                        $gestionProceso2->tipificacion_id=$form->tipificacion_id2;
                        if(!empty($form->otro2)){
                            $gestionProceso2->motivo_otro=$form->otro2;
                        }
                        $gestionProceso2->backoffice_id=$backoffice_id;
                        $gestionProceso2->gestion_id=$gestion->id;
                        $gestionProceso2->tipo_gestion=7;//el tipo de gestion para correo es 7
                        $gestionProceso2->save();
                    }
                    
                }
                      
            }
        DB::commit();   
        } catch (\Exception $e) {
        DB::rollback();
        dd($e);
    }
        
    }
}
