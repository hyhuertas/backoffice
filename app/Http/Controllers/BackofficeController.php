<?php

namespace App\Http\Controllers;

use App\Models\CambioPlan;
use App\Models\Gestion;
use App\Models\GestionAnillo;
use App\Models\Inconsistencia;
use App\Models\LogGestion;
use App\Models\CierrePqr;
use App\Models\Usuario;
use App\Models\CasoEspecial;
use App\Models\Retenido;
use App\Models\GestionProceso;
use App\Models\Correo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class BackofficeController extends Controller implements FromView
{
    public function verGestiones(){
        //se define el rol para asignarlo a cada uno de los backoffice y asi porder separarlos y concederle los permisos necesarios para el ingreso al modulo
        $rol=auth()->user()->rol_user_id;
        // si el rol es cambio de plan
        
        return view('backoffice.backoffice-listarGestion')->with('backoffice.backoffice-correo');
        
    }
    public function cargarExcel(){
        return view('backoffice.backoffice-cargar-excel');
    }
    public function cargarExcelAdicional(){
        return view('backoffice.backoffice-cargar-excel-adicional');
    }
    
    public function listarGestion(Request $request){


        $rol=auth()->user()->rol_user_id;
        $backoffice_id=auth()->user()->codigo_usercrm;
        // si el rol es cambio de plan
        if($rol==2){
            $gestiones=CambioPlan::where('fecha_creacion','>=','2019-12-06')->orderBy('idcambio_plan','DESC')->get();
            foreach ($gestiones as  $gestion) {
                
                $gestion->asesor=Usuario::where('usuario',$gestion->usuario_gestiona)->first();
                $gestion->gestion=Gestion::where('gestion_id',$gestion->idcambio_plan)->first();
            }
            $filtro=$gestiones->filter(function($value,$key){
                return $value->gestion==null;
            });
            $gestiones=array_values($filtro->toArray());
            //si el rol es evasiones
        }elseif ($rol==3) {
            $gestiones=GestionAnillo::where('fecha_creacion','>=','2019-12-06')->orderBy('idmaster','DESC')
            ->where('tipificacion','Evasion Llamada')
            ->get();
            foreach ($gestiones as  $gestion) {
                $gestion->asesor=Usuario::where('usuario',$gestion->usuario_agente)->first();
                $gestion->gestion=Gestion::where('gestion_id',$gestion->idmaster)->first();
            }
           
            $filtro=$gestiones->filter(function($value,$key){
                return $value->gestion==null;
            });
            $gestiones=array_values($filtro->toArray());
            //si el rol es cierre pqr
        }elseif ($rol==5) {
            $gestiones=CierrePqr::where('estado_id', null)->get();
            foreach ($gestiones as $gestion) {
                $gestion->gestion=CierrePqr::where('id', $gestion->id)->first();          
            }
            //si el rol es inconsistencias
        }elseif ($rol==4) {
            $gestiones=Inconsistencia::where('backoffice_id',$backoffice_id)
                                     ->where('justificacion_id', null)
                                     ->orderBy('created_at','DESC')->get();
            //si el rol es casos especiales
        }elseif ($rol==6) {
            $gestiones=CasoEspecial::where('estado_id',null)
            ->where('backoffice_id',$backoffice_id)
            ->get();
            //si el rol es retenidos
        }elseif ($rol==7) {
            $gestiones=Retenido::where('estado_id',null)
            ->where('backoffice_id',$backoffice_id)
            ->get();
            //si el rol es correo
        }elseif ($rol==8) {
            $gestiones=Correo::where('backoffice_id',$backoffice_id)
            ->orderBy('id', 'DESC')
            ->get();
            foreach ($gestiones as $gestion) {
                $gestion->gestionProceso=GestionProceso::with('tipificacion')->where('gestion_id',$gestion->id)
                ->where('tipo_gestion',7)->get();
            }          
        }
        return $gestiones;

    }
    /* funcion para el conteo de gestiones */
    public function contarGestion(Request $request){

        $rol=auth()->user()->rol_user_id;
        $backoffice_id=auth()->user()->codigo_usercrm;
        $hoy= Carbon::create('2019-12-06 00:00:00')->format('Y-m-d H:i:s');
        $enero = Carbon::create('2019-01-01 00:00:00');
        $desde=$request->fechaInicio;
        $hasta=$request->fechaFinal;
        $grafica=[];
        $meses=[];
        $categorias=[];
        $mes= Carbon::now()->format('m');
        $anio= Carbon::now()->format('Y');
        $gestiones2=[];
        // si el rol es cambio de plan
        if($rol==2){
            $gestiones_consulta=CambioPlan::where('fecha_creacion','>=',$hoy)->orderBy('idcambio_plan','DESC')->whereYear('fecha_creacion', $anio)->count();      
            $gestiones_consulta2=Gestion::where('tipo_gestion','=',1)->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();             
            $gestiones_consulta3=Gestion::where('tipo_gestion','=',1)->where('created_at','>=',$hoy);     
            $gestiones_consulta4=Gestion::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->where('tipo_gestion','=',1)->whereNotNull('estado_id');   
        //si el rol es evasiones    
        }elseif ($rol==3) {
            $gestiones_consulta=GestionAnillo::where('tipificacion','=','Evasion Llamada')->where('fecha_creacion','>=',$hoy)->whereYear('fecha_creacion', $anio)->count();
            $gestiones_consulta2=Gestion::whereIn('estado_id',[4,5])->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();             
            $gestiones_consulta3=Gestion::where('tipo_gestion','=',2)->where('created_at','>=',$hoy);
            $gestiones_consulta4=Gestion::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->where('tipo_gestion','=',2)->whereNotNull('estado_id');   
            //si el rol es cierre pqr
        }elseif ($rol==5) {
            $gestiones_consulta=CierrePqr::where('backoffice_id',$backoffice_id)->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();
            $gestiones_consulta2=CierrePqr::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();                             
            $gestiones_consulta3=CierrePqr::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy);
            $gestiones_consulta4=CierrePqr::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->whereNotNull('estado_id');
            //si el rol es inconsistencias
        }elseif ($rol==4) {
            $gestiones_consulta=Inconsistencia::where('backoffice_id',$backoffice_id)->whereYear('created_at', $anio)->where('created_at','>=',$hoy)->count();
            $gestiones_consulta2=Inconsistencia::where('backoffice_id',$backoffice_id)->whereNotNull('justificacion_id')->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();                             
            $gestiones_consulta3=Inconsistencia::where('backoffice_id',$backoffice_id)->whereNotNull('justificacion_id')->where('created_at','>=',$hoy);
            $gestiones_consulta4=Inconsistencia::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->whereNotNull('justificacion_id');
            //si el rol es casos especiales
        }elseif ($rol==6) {
            $gestiones_consulta=CasoEspecial::where('backoffice_id',$backoffice_id)->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();
            $gestiones_consulta2=CasoEspecial::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();                                           
            $gestiones_consulta3=CasoEspecial::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy);
            $gestiones_consulta4=CasoEspecial::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->whereNotNull('estado_id');
            //si el rol es retenidos
        }elseif ($rol==7) {
            $gestiones_consulta=Retenido::where('backoffice_id',$backoffice_id)->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count();
            $gestiones_consulta2=Retenido::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy)->count();                            
            $gestiones_consulta3=Retenido::where('backoffice_id',$backoffice_id)->whereNotNull('estado_id')->where('created_at','>=',$hoy);
            $gestiones_consulta4=Retenido::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))->whereNotNull('estado_id');
            //si el rol es correo
        }elseif ($rol==8) {
            $gestiones_consulta=Correo::where('backoffice_id',$backoffice_id)->where('created_at','>=',$hoy)->whereYear('created_at', $anio)->count(); 
            $gestiones_consulta3=Correo::where('backoffice_id',$backoffice_id)->where('created_at','>=',$hoy);
            //si el rol es administrador
        }elseif ($rol==1) {
            $gestiones_consulta=LogGestion::where('created_at', '>=',$hoy)->whereYear('created_at', $anio)->count();  
        }
        /* se recibe la variable contar(true) desde el componente con el metodo post para indicar los arreglos de cada uno de los backoffice y enviarlos a cada uno de ellos*/
        if($request->contar){
            //envio de conteo de gestiones a todos los roles excepto a correo y administrador
            if($rol!=8 && $rol!=1){
            $gestiones[0]['name'] = 'TOTALES';
            $gestiones[0]['data'] = [$gestiones_consulta];
            $gestiones[0]['stack'] = 'stack';
            
            $gestiones[1]['name'] = 'GESTIONADAS';
            $gestiones[1]['data'] = [$gestiones_consulta2];
            $gestiones[1]['stack'] = 'stack';  

            $gestiones2[0]['name'] = 'noviembre'; 
            $gestiones2[0]['data'] = [$gestiones_consulta3->whereYear('created_at', $anio)->count()]; 
            $gestiones2[0]['stack'] = 'stack';

            $datos = $gestiones_consulta4->where('backoffice_id',$backoffice_id)
                ->where('created_at','>=',$hoy)
                ->groupBy(DB::raw("MONTH(created_at)"))
                ->get();
                $categorias[] ="Enero";
                $categorias[] ="Febrero";
                $categorias[] ="Marzo";
                $categorias[] ="Abril";
                $categorias[] ="Mayo";
                $categorias[] ="Junio";
                $categorias[] ="Julio";
                $categorias[] ="Agosto";
                $categorias[] ="Septiembre";
                $categorias[] ="Octubre";
                $categorias[] ="Noviembre";
                $categorias[] ="Diciembre";
                /* asigna el nombre del mes a los arreglos generados*/
                foreach ($categorias as $key1 => $mes) {
                    $num_mes=$key1+1;
                    $backoffice['data'][$key1]=0;
                    foreach ($datos as $key => $d) {
                        if($num_mes==$d->mes){
                            $backoffice['data'][$key1]=$d->cantidad;
                                    break;
                        }
                    }
                }
                $meses[]=$backoffice;
                $backoffice=[];
                $meses[0]['name'] = 'Gestiones';
                $meses[0]['stack'] = 'stack';
                //envio de conteo de gestiones para el rol correo
            }elseif($rol==8){
                $gestiones[0]['name'] = 'Casos Gestionados'; 
                $gestiones[0]['data'] = [$gestiones_consulta]; 
                $gestiones[0]['stack'] = 'stack'; 
                                
                $datos = Correo::select(DB::raw('count(*) as cantidad'), DB::raw("MONTH(created_at) as mes"))
                ->whereYear('created_at', $anio)
                ->where('created_at','>=',$hoy)
                ->where('backoffice_id',$backoffice_id)
                ->groupBy(DB::raw("MONTH(created_at)"))
                ->get();
                $categorias[] ="Enero";
                $categorias[] ="Febrero";
                $categorias[] ="Marzo";
                $categorias[] ="Abril";
                $categorias[] ="Mayo";
                $categorias[] ="Junio";
                $categorias[] ="Julio";
                $categorias[] ="Agosto";
                $categorias[] ="Septiembre";
                $categorias[] ="Octubre";
                $categorias[] ="Noviembre";
                $categorias[] ="Diciembre";
                /* recorrido de meses */
                foreach ($categorias as $key1 => $mes) {
                    $num_mes=$key1+1;                    
                    $backoffice['data'][$key1]=0; // coloca un cero en cada una de los datos en caso de 
                                                  //que no se encuentre nada en la data del mes
                    foreach ($datos as $key => $d) {
                        if($num_mes==$d->mes){
                            /* asignacion de data cada uno de los meses.  */
                            $backoffice['data'][$key1]=$d->cantidad;
                                    break;//rompe el ciclo de llenado de la data para que pase al siguiente mes.
                        }
                    }
                }
                $meses[]=$backoffice;
                $backoffice=[];
                $meses[0]['name'] = 'Correos';
                $meses[0]['stack'] = 'stack';
                //envio de conteo de gestion para el rol administrador
            }elseif ($rol==1) {
                $gestiones[0]['name'] = 'TOTALES'; 
                $gestiones[0]['data'] = [$gestiones_consulta]; 
                $gestiones[0]['stack'] = 'stack';

                $datos = LogGestion::select(DB::raw('count(*) as cantidad'), 'tipo_gestion')
                        ->whereYear('created_at', $anio)
                        ->where('created_at','>=',$hoy)
                        ->groupBy('tipo_gestion')
                        ->get();
                    $backoffice['data']=[];
                    for ($i=7; $i < 7; $i++) {
                        $backoffice['data'][] =0;
                    }
                    foreach ($datos as $key => $d) {
                        $backoffice['data'][]=$d->cantidad;
                    
                    }
                    $grafica[]=$backoffice;
                    $backoffice=[];
                $grafica[0]['name'] = 'Gestiones'; 
                $gestiones2[0]['data'] = [$grafica]; 
                $grafica[0]['stack'] = 'stack';
            }
            

        }        
        //se retornan las variables de entorno para cada uno de los componentes.
        return [
            'gestiones'=>$gestiones,
            'gestiones2'=>$gestiones2,
            'grafica'=>$grafica,
            'meses'=>$meses,
            'nombre'=>$categorias
        ];

    }
    //funcion creada para poder crear la segunda grafica del administrador para poder separarlas por backoffice y filtrarlas por fecha
    public function generarGrafica(Request $request){
        $datos = LogGestion::select(DB::raw('count(*) as cantidad'), 'tipo_gestion')
                        ->where('created_at','>=',$request->fechaInicial.' 00:00:00')
                        ->where('created_at','<=',$request->fechaFinal.' 23:59:59')
                        ->groupBy('tipo_gestion')
                        ->get();
        $grafica=[];
        $backoffice['data']=[];
                    for ($i=7; $i < 7; $i++) {
                        $backoffice['data'][] =0;
                    }
                    foreach ($datos as $key => $d) {
                        $backoffice['data'][]=$d->cantidad;
                    
                    }
                    $grafica[0]['name']=['Gestiones'];
                    $grafica[0]=$backoffice;
                    $backoffice=[];
        return json_encode($grafica);
    }
    public function tipificarGestion(Request $request){
        $rol=auth()->user()->rol_user_id;
        $backoffice_id=auth()->user()->codigo_usercrm;
        $form=(object) $request->model;
        $gestionSegundoAnillo=(object) $request->gestion;
        //aqui procede a almacenar en la base de datos
        
        $tipificacion_id=$form->tipificacion_id;
        $motivo_rechazo=$form->motivo_rechazo;
        DB::beginTransaction();
        try{
              //si el rol es cambio de plan
            if($rol==2){

                $gestion=Gestion::where('gestion_id',$gestionSegundoAnillo->idcambio_plan)->first();
                if(!isset($gestion)){
                    $gestion=new Gestion();
                }
                $gestion->gestion_id=$gestionSegundoAnillo->idcambio_plan;
                $gestion->tipo_gestion=1;
                if(isset($motivo_rechazo)){
                    $gestion->observacion_backoffice=$motivo_rechazo;
                }
                //si el rol es evasiones
            }elseif($rol==3){
                $gestion=Gestion::where('gestion_id',$gestionSegundoAnillo->idmaster)->first();
                if(!isset($gestion)){
                    $gestion=new Gestion();
                }
                $gestion->gestion_id=$gestionSegundoAnillo->idmaster;
                $gestion->tipo_gestion=2;
            }

            $gestion->estado_id=$tipificacion_id; 
            $gestion->asesor_id=$gestionSegundoAnillo->asesor['id_usuario'];
            $gestion->backoffice_id=$backoffice_id;
            $gestion->save();
           if($gestion->id){
               $logGestion=new LogGestion();
               $logGestion->gestion_id=$gestion->id;
               $logGestion->user_id= $backoffice_id;
               $logGestion->rol_id=$rol;
               $logGestion->estado_id=$gestion->estado_id;
               if(isset($gestion->observacion_backoffice)){
                   $logGestion->observacion=$gestion->observacion_backoffice;
               }
               $logGestion->tipo_gestion=$gestion->tipo_gestion;
               $logGestion->save();
           }
        DB::commit();   
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        
        
    }
    public function correo(){

        return view('backoffice.backoffice-correo');
    } 

    public function view(): View
    {
        $crm=$this->backoffice_id;
        $rol=$this->rol_backoffice_id;

        return view('administrador.admin-home', [
            'gestiones' => $this->contarGestion(),
            'gestiones2' => $this->contarGestion()
            ]);
    }
}
