<?php

namespace App\Http\Controllers;

use App\Models\ClasificacionItems;
use Illuminate\Http\Request;


class ClasificacionItemsController extends Controller
{
    public function listarClasificacionItems(Request $request)
    {
        $rol_id=auth()->user()->rol_user_id;
        $Clasificacion_items=ClasificacionItems::where('activo',true)->orderBy('id','asc');
        
        // si el rol es cambio de plan no puede sali el estado de corregido
        if($rol_id==2){
            $Clasificacion_items=$Clasificacion_items->where('id','<>',3);
        }
        $Clasificacion_items=$Clasificacion_items->get();
        return $this->itemEspecifico($Clasificacion_items,$request->nivel,$request->padre_id,$request->clasificacion_id);
    }
    public function itemEspecifico($array,$nivel,$padre_id,$clasificacion_id){
        $array_peticion=[];
         foreach($array as $items){
             if($items->nivel==$nivel && $clasificacion_id==$items->clasificacion_id && $items->padre_id==$padre_id ){
                 $array_peticion[]=$items;
             }
         }
         return $array_peticion;
     }
}
