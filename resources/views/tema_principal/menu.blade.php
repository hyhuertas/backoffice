
    <v-navigation-drawer class="menu_principal" v-model="drawer" :clipped="$vuetify.breakpoint.lgAndUp" app>
    <v-list dense rounded>
      <template v-for="(item, i) in items">
        <v-list-item v-if="item.text" :key="i" @click="" :to="item.ruta">
          <v-list-item-action>
            <v-icon>@{{ item.icon }}</v-icon>
          </v-list-item-action>
          <v-list-item-content>
            <v-list-item-title>
              @{{ item.text }}
            </v-list-item-title>
          </v-list-item-content>
        </v-list-item>
        <v-divider v-else-if="item.divider" :key="i" dark class="my-3"></v-divider>
      </template>
    </v-list>
  </v-navigation-drawer>
