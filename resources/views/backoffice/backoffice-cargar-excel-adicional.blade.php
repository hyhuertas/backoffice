@extends('layout')
@section('componentes_vue')
<v-content>
    <v-container fluid fill-height class="pa-0 " style="max-width:100%" >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
                <backoffice-cargar-excel-adicional :rol_id={{auth()->user()->rol_user_id}}></backoffice-cargar-excel-adicional>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
    
    @include('tema_principal.bar')
    {{-- si el rol es Cambio de Plan --}}
    @if(auth()->user()->rol_user_id==2)
    @include('backofficeCambioPlan.backoffice-cambio-plan-menu')
    {{-- si el rol es evasiones --}}
    @elseif(auth()->user()->rol_user_id==3)
    @include('backofficeEvasiones.backoffice-evasiones-menu')
    {{-- si el rol es cambio de plan --}}
    @elseif(auth()->user()->rol_user_id==4)
    @include('backofficeInconsistencias.backoffice-inconsistencias-menu')        
    {{-- si el rol es cierre pqr --}}
    @elseif(auth()->user()->rol_user_id==5)
    @include('backofficeCierrePqr.backoffice-cierre-pqr-menu')
    {{-- si el rol es casos especiales --}}
    @elseif(auth()->user()->rol_user_id==6)
    @include('backofficeCasoEspecial.backoffice-caso-especial-menu')    
    {{-- si el rol es retenidos --}}
    @elseif(auth()->user()->rol_user_id==7)
    @include('backofficeRetenido.backoffice-retenido-menu')
    {{--  si el rol es correo  --}}
    @elseif(auth()->user()->rol_user_id==8)
    @include('backofficeCorreo.backoffice-correo-menu')
    @endif
@endsection