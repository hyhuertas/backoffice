@extends('layout')
@section('componentes_vue')
<v-content>
    <v-container fluid fill-height class="pa-0 " style="max-width:100%" >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
               <backoffice-correo :rol_id={{auth()->user()->rol_user_id}}></backoffice-correo>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
    @include('tema_principal.bar')
    {{-- si el rol es correo --}}
    @if (auth()->user()->rol_user_id==8)
        @include('backofficeCorreo.backoffice-correo-menu')
    @endif

@endsection