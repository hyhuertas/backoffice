@extends('layout')
@section('componentes_vue')
<v-content>
    <v-container fluid fill-height class="pa-0 " style="max-width:100%" >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
            <backoffice-listar-gestion-estado :opc="{{($opc)}}" :array_gestiones="{{$gestiones}}" :rol_id={{auth()->user()->rol_user_id}}></backoffice-listar-gestion-estado>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
    @include('tema_principal.bar')
    {{-- si el rol es cambio de plan --}}
    @if (auth()->user()->rol_user_id==2)
        @include('backofficeCambioPlan.backoffice-cambio-plan-menu')
    {{-- si el backoffice es evasiones --}}
    @elseif(auth()->user()->rol_user_id==3)
        @include('backofficeEvasiones.backoffice-evasiones-menu')
    {{-- Si el backoffice es cierrepqr --}}
    @elseif(auth()->user()->rol_user_id==5)
        @include('backofficeCierrePqr.backoffice-cierre-pqr-menu')
    {{-- si el rol es de inconsistencias --}}
    @elseif(auth()->user()->rol_user_id==4)
        @include('backofficeInconsistencias.backoffice-inconsistencias-menu')
    {{-- si el rol es de caso especial --}}
    @elseif(auth()->user()->rol_user_id==6)
        @include('backofficeCasoEspecial.backoffice-caso-especial-menu')
    {{-- si el rol es retenido --}}
    @elseif(auth()->user()->rol_user_id==7)
        @include('backofficeRetenido.backoffice-retenido-menu')
    @elseif(auth()->user()->rol_user_id==8)
        @include('backofficeCorreo.backoffice-correo-menu')
    @endif
   
@endsection