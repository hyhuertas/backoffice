<table>
    <thead>
        <tr>
            <td colspan="39" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="3" style="text-align:center; color: ##ff4747; background: #a2ff99;">{{ strtoupper('Gestion') }}</td>            
        </tr>
        <tr>
            <td>idmaster</td>
            <td>fecha_creacion</td>
            <td>usuario_agente</td>
            <td>tipo_gestion</td>
            <td>contrato</td>
            <td>min_ac</td>
            <td>ucid</td>
            <td>custcode</td>
            <td>nombre_cliente</td>
            <td>cedula_cliente</td>
            <td>corte_linea</td>
            <td>tipificacion</td>
            <td>tipificacion2</td>
            <td>tipificacion3</td>
            <td>tipificacion4</td>
            <td>motivo de cancelacion</td>
            <td>observaciones de motivo de cancelacion</td>
            <td>solicita_ajuste</td>
            <td>valor_ajuste</td>
            <td>call_transfiere</td>
            <td>id_plan_actual</td>
            <td>plan_actual</td>
            <td>precio_actual</td>
            <td>id_plan_nuevo</td>
            <td>plan_nuevo</td>
            <td>precio_nuevo</td>
            <td>valor_diferenciado</td>
            <td>indicador_erosion</td>
            <td>fecha_corte</td>
            <td>observacion1</td>
            <td>observacion2</td>
            <td>Usuario que genera la evasion</td>
            <td>Fecha Evasion</td>
            <td>Formato de Novedades Call Center</td>
            <td>Tipificacion Erosion</td>
            <td>Plan actual erosion</td>
            <td>Precio actual erosion</td>
            <td>Plan nuevo erosion</td>
            <td>Precio nuevo erosion</td>
            <td>Tipificacion</td>
            <td>Motivo otro</td>
            <td>Usuario</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $key => $gestion)
            <tr>
                <th>{{ is_null($gestion->idmaster) ? 'No Aplica' : $gestion->idmaster}}</th>
                <th>{{ is_null($gestion->fecha) ? 'No Aplica' : $gestion->fecha}}</th>
                <th>{{ is_null($gestion->usuario_agente) ? 'No Aplica' : $gestion->usuario_agente}}</th>
                <th>{{ is_null($gestion->tipo_gestion) ? 'No Aplica' : $gestion->tipo_gestion}}</th>
                <th>{{ is_null($gestion->contrato) ? 'No Aplica' : $gestion->contrato}}</th>
                <th>{{ is_null($gestion->min_ac) ? 'No Aplica' : $gestion->min_ac}}</th>
                <th>{{ is_null($gestion->ucid) ? 'No Aplica' : $gestion->ucid}}</th>
                <th>{{ is_null($gestion->custcode) ? 'No Aplica' : $gestion->custcode}}</th>
                <th>{{ is_null($gestion->nombre_cliente) ? 'No Aplica' : $gestion->nombre_cliente}}</th>
                <th>{{ is_null($gestion->cedula_cliente) ? 'No Aplica' : $gestion->cedula_cliente}}</th>
                <th>{{ is_null($gestion->corte_linea) ? 'No Aplica' : $gestion->corte_linea}}</th>
                <th>{{ is_null($gestion->tipificacion) ? 'No Aplica' : $gestion->tipificacion}}</th>
                <th>{{ is_null($gestion->tipificacion2) ? 'No Aplica' : $gestion->tipificacion2}}</th>
                <th>{{ is_null($gestion->tipificacion3) ? 'No Aplica' : $gestion->tipificacion3}}</th>
                <th>{{ is_null($gestion->tipificacion4) ? 'No Aplica' : $gestion->tipificacion4}}</th>
                <th>{{ is_null($gestion->motivo_cancelacion) ? 'No Aplica' : $gestion->motivo_cancelacion}}</th>
                <th>{{ is_null($gestion->observacion_cancelacion) ? 'No Aplica' : $gestion->observacion_cancelacion}}</th>
                <th>{{ is_null($gestion->solicita_ajuste) ? 'No Aplica' : $gestion->solicita_ajuste}}</th>
                <th>{{ is_null($gestion->valor_ajuste) ? 'No Aplica' : $gestion->valor_ajuste}}</th>
                <th>{{ is_null($gestion->call_transfiere) ? 'No Aplica' : $gestion->call_transfiere}}</th>
                <th>{{ is_null($gestion->id_plan_actual) ? 'No Aplica' : $gestion->id_plan_actual}}</th>
                <th>{{ is_null($gestion->plan_actual) ? 'No Aplica' : $gestion->plan_actual}}</th>
                <th>{{ is_null($gestion->precio_actual) ? 'No Aplica' : $gestion->precio_actual}}</th>
                <th>{{ is_null($gestion->id_plan_nuevo) ? 'No Aplica' : $gestion->id_plan_nuevo}}</th>
                <th>{{ is_null($gestion->plan_nuevo) ? 'No Aplica' : $gestion->plan_nuevo}}</th>
                <th>{{ is_null($gestion->precio_nuevo) ? 'No Aplica' : $gestion->precio_nuevo}}</th>
                <th>{{ is_null($gestion->valor_diferenciado) ? 'No Aplica' : $gestion->valor_diferenciado}}</th>
                <th>{{ is_null($gestion->indicador_erosion) ? 'No Aplica' : $gestion->indicador_erosion}}</th>
                <th>{{ is_null($gestion->fecha_corte) ? 'No Aplica' : $gestion->fecha_corte}}</th>
                <th>{{ is_null($gestion->observacion1) ? 'No Aplica' : $gestion->observacion1}}</th>
                <th>{{ is_null($gestion->observacion2) ? 'No Aplica' : $gestion->observacion2}}</th>
                <th>{{ is_null($gestion->usuario_evasion) ? 'No Aplica' : $gestion->usuario_evasion}}</th>
                <th>{{ is_null($gestion->fecha_evasion) ? 'No Aplica' : $gestion->fecha_evasion}}</th>
                <th>{{ is_null($gestion->formato_novedades) ? 'No Aplica' : $gestion->formato_novedades}}</th>
                <th>{{ is_null($gestion->tipificacion_erosion) ? 'No Aplica' : $gestion->tipificacion_erosion}}</th>
                <th>{{ is_null($gestion->plan_actual_erosion) ? 'No Aplica' : $gestion->plan_actual_erosion}}</th>
                <th>{{ is_null($gestion->precio_actual_erosion) ? 'No Aplica' : $gestion->precio_actual_erosion}}</th>
                <th>{{ is_null($gestion->plan_nuevo_erosion) ? 'No Aplica' : $gestion->plan_nuevo_erosion}}</th>
                <th>{{ is_null($gestion->precio_nuevo_erosion) ? 'No Aplica' : $gestion->precio_nuevo_erosion}}</th>
                <th>{{ is_null($gestion->estado['item']) ? 'No hay Tipificacion' : $gestion->estado['item']}}</th>
                <th>{{ is_null($gestion->motivo_otro) ? 'No Aplica' : $gestion->motivo_otro}}</th>    
                <th>{{ is_null($gestion['usuario']['nombre']) ? 'No Aplica' : $gestion['usuario']['nombre']}}</th>            
            </tr>
        @endforeach
    </tbody>
</table>