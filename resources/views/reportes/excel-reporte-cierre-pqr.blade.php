<table>
    <thead>
        <tr>
            <td colspan="23" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="2" style="text-align:center; color: ##ff4747; background: #a2ff99;">{{ strtoupper('Gestion') }}</td>            
        </tr>
        <tr>
            <td>NR/CUN</td>
            <td>Fecha Recibido</td>
            <td>Días SAP</td>
            <td>Tipo PQR</td>
            <td>Tipo Petición</td>
            <td>Tipo Reclamo</td>
            <td>Subreclamo</td>
            <td>Estado</td>
            <td>Anexos</td>
            <td>Remitente</td>
            <td>Radicado Por</td>
            <td>Área Responsable</td>
            <td>Analista Responsable</td>
            <td>MIN</td>
            <td>Fecha Silencio</td>
            <td>Consecutivo Salida</td>
            <td>Área Radicación</td>
            <td>ID</td>
            <td>Región</td>
            <td>Dirección</td>
            <td>Ciudad</td>
            <td>Distribuidor</td>
            <td>Ciclo Fact</td>
            <td>Tipificacion</td>
            <td>Usuario</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $key => $gestion)
            <tr>
                <th>{{ is_null($gestion->nr_cun) ? 'No Aplica' : $gestion->nr_cun }}</th>
                <th>{{ is_null($gestion->fecha_recibido) ? 'No Aplica' : $gestion->fecha_recibido }}</th>
                <th>{{ is_null($gestion->dias_sap) ? 'No Aplica' : $gestion->dias_sap }}</th>
                <th>{{ is_null($gestion->tipo_pqr) ? 'No Aplica' : $gestion->tipo_pqr }}</th>
                <th>{{ is_null($gestion->tipo_peticion) ? 'No Aplica' : $gestion->tipo_peticion }}</th>
                <th>{{ is_null($gestion->tipo_reclamo) ? 'No Aplica' : $gestion->tipo_reclamo }}</th>
                <th>{{ is_null($gestion->subreclamo) ? 'No Aplica' : $gestion->subreclamo }}</th>
                <th>{{ is_null($gestion->estado) ? 'No Aplica' : $gestion->estado }}</th>
                <th>{{ is_null($gestion->anexos) ? 'No Aplica' : $gestion->anexos }}</th>
                <th>{{ is_null($gestion->remitente) ? 'No Aplica' : $gestion->remitente }}</th>
                <th>{{ is_null($gestion->radicaco_por) ? 'No Aplica' : $gestion->radicaco_por }}</th>
                <th>{{ is_null($gestion->area_responsable) ? 'No Aplica' : $gestion->area_responsable }}</th>
                <th>{{ is_null($gestion->analista_responsable) ? 'No Aplica' : $gestion->analista_responsable }}</th>
                <th>{{ is_null($gestion->min) ? 'No Aplica' : $gestion->min }}</th>
                <th>{{ is_null($gestion->fecha_silencio) ? 'No Aplica' : $gestion->fecha_silencio }}</th>
                <th>{{ is_null($gestion->consecutivo_salida) ? 'No Aplica' : $gestion->consecutivo_salida }}</th>
                <th>{{ is_null($gestion->area_radicacion) ? 'No Aplica' : $gestion->area_radicacion }}</th>
                <th>{{ is_null($gestion->codigo_id) ? 'No Aplica' : $gestion->codigo_id }}</th>
                <th>{{ is_null($gestion->region) ? 'No Aplica' : $gestion->region }}</th>
                <th>{{ is_null($gestion->direccion) ? 'No Aplica' : $gestion->direccion }}</th>
                <th>{{ is_null($gestion->ciudad) ? 'No Aplica' : $gestion->ciudad }}</th>
                <th>{{ is_null($gestion->distribuidor) ? 'No Aplica' : $gestion->distribuidor }}</th>
                <th>{{ is_null($gestion->ciclo_fact) ? 'No Aplica' : $gestion->ciclo_fact }}</th>
                <th>{{ is_null($gestion['tipificacion']['item']) ? 'No Aplica' : $gestion['tipificacion']['item'] }}</th>
                <th>{{ is_null($gestion['usuario']['nombre']) ? 'No Aplica' : $gestion['usuario']['nombre'] }}</th>
            </tr>
        @endforeach
    </tbody>
</table>