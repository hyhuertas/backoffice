<table>
    <thead>
        <tr>
            <td colspan="9" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="3" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Gestion') }}</td>
            <td colspan="14" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Formulario 1') }}</td>
            <td colspan="14" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Formulario 2') }}</td>
        </tr>
        <tr>
            <td style="text-align:center; color: #4CAF50">codigo_unico</td>
            <td style="text-align:center; color: #4CAF50">fecha</td>
            <td style="text-align:center; color: #4CAF50">custcode</td>
            <td style="text-align:center; color: #4CAF50">tipo</td>
            <td style="text-align:center; color: #4CAF50">inconsistencia</td>
            <td style="text-align:center; color: #4CAF50">cod_user</td>
            <td style="text-align:center; color: #4CAF50">nombre_consultor</td>
            <td style="text-align:center; color: #4CAF50">ubicacion</td>
            <td style="text-align:center; color: #4CAF50">gerencia</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Aplica Ajuste</td> 
            <td style="text-align:center; color: #4CAF50">Ajuste Compartido</td> 
            <td style="text-align:center; color: #4CAF50">Fecha inconsistencia</td>           
            <td style="text-align:center; color: #4CAF50">Fecha reporte</td>
            <td style="text-align:center; color: #4CAF50">Min</td>
            <td style="text-align:center; color: #4CAF50">Custode</td>
            <td style="text-align:center; color: #4CAF50">Customer_id</td>
            <td style="text-align:center; color: #4CAF50">Valor sin iva</td>
            <td style="text-align:center; color: #4CAF50">Total</td>
            <td style="text-align:center; color: #4CAF50">Rta Caso</td>
            <td style="text-align:center; color: #4CAF50">Responsable</td>
            <td style="text-align:center; color: #4CAF50">User</td>
            <td style="text-align:center; color: #4CAF50">Estado</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Motivo Otro</td>
            <td style="text-align:center; color: #4CAF50">Usuario</td>
            <td style="text-align:center; color: #4CAF50">Fecha inconsistencia 2</td>           
            <td style="text-align:center; color: #4CAF50">Fecha reporte 2</td>
            <td style="text-align:center; color: #4CAF50">Min 2</td>
            <td style="text-align:center; color: #4CAF50">Custode 2</td>
            <td style="text-align:center; color: #4CAF50">Customer_id 2</td>
            <td style="text-align:center; color: #4CAF50">Valor sin iva 2</td>
            <td style="text-align:center; color: #4CAF50">Total 2</td>
            <td style="text-align:center; color: #4CAF50">Rta Caso 2</td>
            <td style="text-align:center; color: #4CAF50">Responsable 2</td>
            <td style="text-align:center; color: #4CAF50">User 2</td>
            <td style="text-align:center; color: #4CAF50">Estado 2</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion 2</td>
            <td style="text-align:center; color: #4CAF50">Motivo Otro 2</td>
            <td style="text-align:center; color: #4CAF50">Usuario 2</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $key => $gestion)
            <tr>
                <th>{{$gestion->codigo_unico}}</th>
                <th>{{$gestion->fecha}}</th>
                <th>{{$gestion->custcode}}</th>
                <th>{{$gestion->tipo}}</th>
                <th>{{$gestion->inconsistencia}}</th>
                <th>{{$gestion->cod_user}}</th>
                <th>{{$gestion->nombre_consultor}}</th>
                <th>{{$gestion->ubicacion}}</th>
                <th>{{$gestion->gerencia}}</th>
                <th>{{ is_null($gestion->estado['item']) ? 'No Aplica' : $gestion->estado['item']}}</th>
                <th>{{ is_null($gestion->aplica_ajuste) ? 'No Aplica' : $gestion->aplica_ajuste}}</th>
                <th>{{ is_null($gestion->ajuste_compartido) ? 'No Aplica' : $gestion->ajuste_compartido}}</th> 
                @foreach ($gestion->gestionProceso as $i => $proceso)  
                <th>{{ $proceso->fecha_inconsistencia }}</th>                                                                              
                <th>{{ $proceso->fecha_reporte }}</th>
                <th>{{ $proceso->min }}</th>
                <th>{{ $proceso->custcode }}</th>
                <th>{{ $proceso->customer_id }}</th>
                <th>{{ $proceso->valor_sin_iva }}</th>
                <th>{{ $proceso->total }}</th>
                <th>{{ $proceso->rta_caso }}</th>
                <th>{{ $proceso->responsable }}</th>
                <th>{{ $proceso->user }}</th>
                <th>{{ $proceso->estado }}</th>
                <th>{{ $proceso->tipificacion->item }}</th>
                <th>{{ $proceso->motivo_otro }}</th>
                <th>{{ $proceso->usuario->nombre }}</th>                                 
                @endforeach  
            </tr> 
        @endforeach 
    </tbody>
</table>