<table>
    <thead>
        <tr>
            <td colspan="35" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Data Segundo Anillo') }}</td>
            <td colspan="4" style="text-align:center; color: #2a502b; background: #93ff2e;">{{ strtoupper('Gestion backoffice') }}</td>
        </tr>
        <tr>
            <td style="text-align:center; color: #4CAF50">Fecha creacion</td>
            <td style="text-align:center; color: #4CAF50">Usuario Agente</td>
            <td style="text-align:center; color: #4CAF50">Contrato</td>
            <td style="text-align:center; color: #4CAF50">Min AC</td>
            <td style="text-align:center; color: #4CAF50">Min2</td>
            <td style="text-align:center; color: #4CAF50">ucid</td>
            <td style="text-align:center; color: #4CAF50">custcode</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion2</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion3</td> 
            <td style="text-align:center; color: #4CAF50">Tipificacion4</td> 
            <td style="text-align:center; color: #4CAF50">Call Transfiere</td> 
            <td style="text-align:center; color: #4CAF50">Plan actual</td>           
            <td style="text-align:center; color: #4CAF50">Otro Plan actual</td>
            <td style="text-align:center; color: #4CAF50">Plan nuevo</td>
            <td style="text-align:center; color: #4CAF50">Fecha corte</td>
            <td style="text-align:center; color: #4CAF50">Observacion1</td>
            <td style="text-align:center; color: #4CAF50">Observacion2</td>
            <td style="text-align:center; color: #4CAF50">Formato call</td>
            <td style="text-align:center; color: #4CAF50">User transfiere</td>
            <td style="text-align:center; color: #4CAF50">Fecha evacion</td>
            <td style="text-align:center; color: #4CAF50">Nombre cliente</td>
            <td style="text-align:center; color: #4CAF50">Cedula cliente</td>
            <td style="text-align:center; color: #4CAF50">Corte Linea</td>
            <td style="text-align:center; color: #4CAF50">Solicita ajuste</td>
            <td style="text-align:center; color: #4CAF50">Valor ajuste</td>
            <td style="text-align:center; color: #4CAF50">Motivo Cancelacion</td>
            <td style="text-align:center; color: #4CAF50">Observaciones Motivo Cancelacion</td>
            <td style="text-align:center; color: #4CAF50">Cance D</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion erosion</td>
            <td style="text-align:center; color: #4CAF50">Plan actual erosion</td>
            <td style="text-align:center; color: #4CAF50">Plan nuevo erosion</td>
            <td style="text-align:center; color: #4CAF50">Fecha gestion Admin</td>
            <td style="text-align:center; color: #4CAF50">Id cav</td>
            <td style="text-align:center; color: #4CAF50">Otro no retenido</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion Backoffice</td>
            <td style="text-align:center; color: #4CAF50">Observacion asesor</td>
            <td style="text-align:center; color: #4CAF50">Usuario Backoffice</td>
            <td style="text-align:center; color: #4CAF50">Observacion Backoffice</td>
            
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $gestion)
            <tr>
                <th>{{ is_null($gestion->evasiones['fecha_creacion']) ? 'NO APLICA' : $gestion->evasiones['fecha_creacion']}}</th>
                <th>{{ !isset($gestion->evasiones['usuario_crm']) ? 'NO APLICA' : $gestion->evasiones['usuario_crm']}}</th>
                <th>{{ !isset($gestion->evasiones['contrato']) ? 'NO APLICA' : $gestion->evasiones['contrato']}}</th>
                <th>{{ !isset($gestion->evasiones['min_ac']) ? 'NO APLICA' : $gestion->evasiones['min_ac']}}</th>
                <th>{{ !isset($gestion->evasiones['min2']) ? 'NO APLICA' : $gestion->evasiones['min2']}}</th>
                <th>{{ !isset($gestion->evasiones['ucid']) ? 'NO APLICA' : $gestion->evasiones['ucid']}}</th>
                <th>{{ !isset($gestion->evasiones['custcode']) ? 'NO APLICA' : $gestion->evasiones['custcode']}}</th>
                <th>{{ !isset($gestion->evasiones['tipificacion']) ? 'NO APLICA' : $gestion->evasiones['tipificacion']}}</th>
                <th>{{ !isset($gestion->evasiones['tipificacion2']) ? 'NO APLICA' : $gestion->evasiones['tipificacion2']}}</th>
                <th>{{ !isset($gestion->evasiones['tipificacion3']) ? 'NO APLICA' : $gestion->evasiones['tipificacion3']}}</th>
                <th>{{ !isset($gestion->evasiones['tipificacion4']) ? 'NO APLICA' : $gestion->evasiones['tipificacion4']}}</th>
                <th>{{ !isset($gestion->evasiones['call_transfiere']) ? 'NO APLICA' : $gestion->evasiones['call_transfiere']}}</th>
                <th>{{ !isset($gestion->evasiones['plan_actual']) ? 'NO APLICA' : $gestion->evasiones['plan_actual']}}</th>
                <th>{{ !isset($gestion->evasiones['otro_plan_actual']) ? 'NO APLICA' : $gestion->evasiones['otro_plan_actual']}}</th>
                <th>{{ !isset($gestion->evasiones['plan_nuevo']) ? 'NO APLICA' : $gestion->evasiones['plan_nuevo']}}</th>
                <th>{{ !isset($gestion->evasiones['fecha_corte']) ? 'NO APLICA' : $gestion->evasiones['fecha_corte']}}</th>
                <th>{{ !isset($gestion->evasiones['observacion1']) ? 'NO APLICA' : $gestion->evasiones['observacion1']}}</th>
                <th>{{ !isset($gestion->evasiones['observacion2']) ? 'NO APLICA' : $gestion->evasiones['observacion2']}}</th>
                <th>{{ !isset($gestion->evasiones['formato_call']) ? 'NO APLICA' : $gestion->evasiones['formato_call']}}</th>
                <th>{{ !isset($gestion->evasiones['user_trasnfiere']) ? 'NO APLICA' : $gestion->evasiones['user_trasnfiere']}}</th>
                <th>{{ !isset($gestion->evasiones['fecha_evacion']) ? 'NO APLICA' : $gestion->evasiones['fecha_evacion']}}</th>
                <th>{{ !isset($gestion->evasiones['nombre_cliente']) ? 'NO APLICA' : $gestion->evasiones['nombre_cliente']}}</th>
                <th>{{ !isset($gestion->evasiones['cedula_cliente']) ? 'NO APLICA' : $gestion->evasiones['cedula_cliente']}}</th>
                <th>{{ !isset($gestion->evasiones['corte_linea']) ? 'NO APLICA' : $gestion->evasiones['corte_linea']}}</th>
                <th>{{ !isset($gestion->evasiones['solicita_ajuste']) ? 'NO APLICA' : $gestion->evasiones['solicita_ajuste']}}</th>
                <th>{{ !isset($gestion->evasiones['valor_ajuste']) ? 'NO APLICA' : $gestion->evasiones['valor_ajuste']}}</th>
                <th>{{ !isset($gestion->evasiones['motivo_cancelacion']) ? 'NO APLICA' : $gestion->evasiones['motivo_cancelacion']}}</th>
                <th>{{ !isset($gestion->evasiones['observaciones_motivo_cancelacion']) ? 'NO APLICA' : $gestion->evasiones['observaciones_motivo_cancelacion']}}</th>
                <th>{{ !isset($gestion->evasiones['cance_d']) ? 'NO APLICA' : $gestion->evasiones['cance_d']}}</th>
                <th>{{ !isset($gestion->evasiones['tipificacion_erosion']) ? 'NO APLICA' : $gestion->evasiones['tipificacion_erosion']}}</th>
                <th>{{ !isset($gestion->evasiones['plan_actual_erosion']) ? 'NO APLICA' : $gestion->evasiones['plan_actual_erosion']}}</th>
                <th>{{ !isset($gestion->evasiones['plan_nuevo_erosion']) ? 'NO APLICA' : $gestion->evasiones['plan_nuevo_erosion']}}</th>
                <th>{{ !isset($gestion->evasiones['fechagestionadmin']) ? 'NO APLICA' : $gestion->evasiones['fechagestionadmin']}}</th>
                <th>{{ !isset($gestion->evasiones['id_cav']) ? 'NO APLICA' : $gestion->evasiones['id_cav']}}</th>
                <th>{{ !isset($gestion->evasiones['otro_no_retenido']) ? 'NO APLICA' : $gestion->evasiones['otro_no_retenido']}}</th>
                <th>{{ !isset($gestion->estado->item) ? 'SIN TIPIFICAR' : $gestion->estado->item}}</th>
                <th>{{ !isset($gestion->observacion_asesor) ? 'NO APLICA' : $gestion->observacion_asesor}}</th>
                <th>{{ !isset($gestion->usuarioBackoffice->nombre) ? 'NO APLICA' : $gestion->usuarioBackoffice->nombre}}</th>                 
                <th>{{ !isset($gestion->observacion_backoffice) ? 'NO APLICA' : $gestion->observacion_backoffice}}</th>
            </tr> 
        @endforeach 
    </tbody>
</table>