<table>
    <thead>
        <tr>
            <td colspan="11" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Gestion Primaria') }}</td>
            <td colspan="13" style="text-align:center; color: #2a502b; background: #fedb9a;">{{ strtoupper('Formulario 1') }}</td>
            <td colspan="13" style="text-align:center; color: #2a502b; background: #f2ffa8;">{{ strtoupper('Formulario 2') }}</td>
        </tr>
        <tr>
            <td style="text-align:center; color: #4CAF50">Min</td>
            <td style="text-align:center; color: #4CAF50">Custcode</td>
            <td style="text-align:center; color: #4CAF50">Numero PQR</td>
            <td style="text-align:center; color: #4CAF50">Fecha recibido</td>
            <td style="text-align:center; color: #4CAF50">Fecha Respuesta</td>
            <td style="text-align:center; color: #4CAF50">Remitente</td>
            <td style="text-align:center; color: #4CAF50">Respuesta</td>
            <td style="text-align:center; color: #4CAF50">Asunto</td>
            <td style="text-align:center; color: #4CAF50">Aplica ajuste</td>
            <td style="text-align:center; color: #4CAF50">Ajuste Compartido</td>
            <td style="text-align:center; color: #4CAF50">Usuario Gestion</td>
            <td style="text-align:center; color: #4CAF50">Fecha inconsistencia</td>           
            <td style="text-align:center; color: #4CAF50">Fecha reporte</td>
            <td style="text-align:center; color: #4CAF50">Min</td>
            <td style="text-align:center; color: #4CAF50">Custode</td>
            <td style="text-align:center; color: #4CAF50">Customer_id</td>
            <td style="text-align:center; color: #4CAF50">Valor sin iva</td>
            <td style="text-align:center; color: #4CAF50">Total</td>
            <td style="text-align:center; color: #4CAF50">Rta Caso</td>
            <td style="text-align:center; color: #4CAF50">Responsable</td>
            <td style="text-align:center; color: #4CAF50">User</td>
            <td style="text-align:center; color: #4CAF50">Estado</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Motivo Otro</td>
            <td style="text-align:center; color: #4CAF50">Fecha inconsistencia 2</td>           
            <td style="text-align:center; color: #4CAF50">Fecha reporte 2</td>
            <td style="text-align:center; color: #4CAF50">Min 2</td>
            <td style="text-align:center; color: #4CAF50">Custode 2</td>
            <td style="text-align:center; color: #4CAF50">Customer_id 2</td>
            <td style="text-align:center; color: #4CAF50">Valor sin iva 2</td>
            <td style="text-align:center; color: #4CAF50">Total 2</td>
            <td style="text-align:center; color: #4CAF50">Rta Caso 2</td>
            <td style="text-align:center; color: #4CAF50">Responsable 2</td>
            <td style="text-align:center; color: #4CAF50">User 2</td>
            <td style="text-align:center; color: #4CAF50">Estado 2</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion 2</td>
            <td style="text-align:center; color: #4CAF50">Motivo Otro 2</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $key => $gestion)
            <tr>
                <th>{{$gestion->min}}</th>
                <th>{{$gestion->custcode}}</th>
                <th>{{$gestion->numero_pqr}}</th>
                <th>{{$gestion->fecha_recibido}}</th>
                <th>{{$gestion->fecha_respuesta}}</th>
                <th>{{$gestion->remitente}}</th>
                <th>{{$gestion->respuesta}}</th>
                <th>{{$gestion->asunto}}</th>
                <th>{{ is_null($gestion->aplica_ajuste) ? 'No Aplica' : $gestion->aplica_ajuste}}</th>
                <th>{{ is_null($gestion->ajuste_compartido) ? 'No Aplica' : $gestion->ajuste_compartido}}</th>                    
                <th>{{ is_null($gestion->usuario->nombre) ? 'No Aplica' : $gestion->usuario->nombre}}</th> 
                @if (isset($gestion->gestionProceso))
                @foreach ($gestion->gestionProceso as $i => $proceso)  
                <th>{{ $proceso->fecha_inconsistencia }}</th>                                                                              
                <th>{{ $proceso->fecha_reporte }}</th>
                <th>{{ $proceso->min }}</th>
                <th>{{ $proceso->custcode }}</th>
                <th>{{ $proceso->customer_id }}</th>
                <th>{{ $proceso->valor_sin_iva }}</th>
                <th>{{ $proceso->total }}</th>
                <th>{{ $proceso->rta_caso }}</th>
                <th>{{ $proceso->responsable }}</th>
                <th>{{ $proceso->user }}</th>
                <th>{{ $proceso->estado }}</th>
                <th>{{ $proceso->tipificacion->item }}</th>
                <th>{{ $proceso->motivo_otro }}</th>                               
                @endforeach  
                @else
                <th>{{ 'No Aplica' }}</th>                                                                              
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>
                <th>{{ 'No Aplica' }}</th>           
                @endif
                
            </tr> 
        @endforeach 
    </tbody>
</table>