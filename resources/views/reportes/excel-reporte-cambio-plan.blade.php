<table>
    <thead>
        <tr>
            <td colspan="15" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="4" style="text-align:center; color: #2a502b; background: #93ff2e;">{{ strtoupper('Gestion') }}</td>
        </tr>
        <tr>
            <td style="text-align:center; color: #4CAF50">usuario gestiona</td>
            <td style="text-align:center; color: #4CAF50">fecha corte plan</td>
            <td style="text-align:center; color: #4CAF50">plan nuevo plan</td>
            <td style="text-align:center; color: #4CAF50">plan actual plan</td>
            <td style="text-align:center; color: #4CAF50">contrato plan</td>
            <td style="text-align:center; color: #4CAF50">min ac plan</td>
            <td style="text-align:center; color: #4CAF50">min2 plan</td>
            <td style="text-align:center; color: #4CAF50">ucid plan</td>
            <td style="text-align:center; color: #4CAF50">custcode2</td>
            <td style="text-align:center; color: #4CAF50">fecha creacion</td>
            <td style="text-align:center; color: #4CAF50">tipo plan nuevo</td> 
            <td style="text-align:center; color: #4CAF50">tipo plan actual</td> 
            <td style="text-align:center; color: #4CAF50">otro plan actual</td> 
            <td style="text-align:center; color: #4CAF50">cambio plancol</td>           
            <td style="text-align:center; color: #4CAF50">modalidad plan</td>
            <td style="text-align:center; color: #4CAF50">tipificacion</td>
            <td style="text-align:center; color: #4CAF50">observacion asesor</td>
            <td style="text-align:center; color: #4CAF50">observacion backoffice</td>
            <td style="text-align:center; color: #4CAF50">usuario backoffice</td>
        </tr>
    </thead>
    <tbody>
        
        @foreach ($gestiones as $gestion)
            <tr>                  
                <th>{{ is_null($gestion->usuarioAsesor->nombre_usuario. ' ' .$gestion->usuarioAsesor->apellido_usuario) ? 'NO APLICA' :  $gestion->usuarioAsesor->nombre_usuario. ' ' .$gestion->usuarioAsesor->apellido_usuario}}</th>
                <th>{{ !isset($gestion->cambio_plan['fecha_corte_plan']) ? 'NO APLICA' : $gestion->cambio_plan['fecha_corte_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['plan_nuevo_plan']) ? 'NO APLICA' : $gestion->cambio_plan['plan_nuevo_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['plan_actual_plan']) ? 'NO APLICA' : $gestion->cambio_plan['plan_actual_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['contrato_plan']) ? 'NO APLICA' : $gestion->cambio_plan['contrato_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['min_ac_plan']) ? 'NO APLICA' : $gestion->cambio_plan['min_ac_plan'] }}</th>
                <th>{{ empty($gestion->cambio_plan['min2_plan']) ? 'NO APLICA' : $gestion->cambio_plan['min2_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['ucid_plan']) ? 'NO APLICA' : $gestion->cambio_plan['ucid_plan'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['custcode2']) ? 'NO APLICA' : $gestion->cambio_plan['custcode2'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['fecha_creacion']) ? 'NO APLICA' : $gestion->cambio_plan['fecha_creacion'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['tipo_plan_nuevo']) ? 'NO APLICA' : $gestion->cambio_plan['tipo_plan_nuevo'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['tipo_plan_actual']) ? 'NO APLICA' : $gestion->cambio_plan['tipo_plan_actual'] }}</th>
                <th>{{ empty($gestion->cambio_plan['otro_plan_actual']) ? 'NO APLICA' : $gestion->cambio_plan['otro_plan_actual'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['cambio_plancol']) ? 'NO APLICA' : $gestion->cambio_plan['cambio_plancol'] }}</th>
                <th>{{ !isset($gestion->cambio_plan['modalidad_plan']) ? 'NO APLICA' : $gestion->cambio_plan['modalidad_plan'] }}</th>  
                <th>{{ !isset($gestion->estado->item) ? 'NO APLICA' : $gestion->estado->item }}</th>
                <th>{{ !isset($gestion->observacion_asesor) ? 'NO APLICA' : $gestion->observacion_asesor }}</th>
                <th>{{ !isset($gestion->observacion_backoffice) ? 'NO APLICA' : $gestion->observacion_backoffice }}</th>
                <th>{{ !isset($gestion->usuarioBackoffice->nombre) ? 'NO APLICA' : $gestion->usuarioBackoffice->nombre }}</th>
            </tr>
        @endforeach 
    </tbody>
</table>