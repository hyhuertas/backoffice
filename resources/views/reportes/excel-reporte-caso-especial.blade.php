<table>
    <thead>
        <tr>
            <td colspan="15" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="3" style="text-align:center; color: ##ff4747; background: #a2ff99;">{{ strtoupper('Gestion') }}</td>            
        </tr>
        <tr>
            <td>Numero caso</td>
            <td>Fecha Registro</td>
            <td>Fecha Cierre</td>
            <td>Estado</td>
            <td>Usuario Generador</td>
            <td>Usuario Asignado</td>
            <td>Observaciones</td>
            <td>Respuesta</td>
            <td>Nombre Cliente</td>
            <td>Motivo Solicitud</td>
            <td>MIN</td>
            <td>Fecha Comunicacion</td>
            <td>Custcode</td>
            <td>User AC</td>
            <td>Descripcion Propuesta</td>
            <td>Tipificacion</td>
            <td>Motivo pendiente/en espera</td>
            <td>Usuario</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $key => $gestion)
            <tr>
                <th>{{ is_null($gestion->numero_caso) ? 'No Aplica' : $gestion->numero_caso}}</th>
                <th>{{ is_null($gestion->fecha_registro) ? 'No Aplica' : $gestion->fecha_registro}}</th>
                <th>{{ is_null($gestion->fecha_cierre) ? 'No Aplica' : $gestion->fecha_cierre}}</th>
                <th>{{ is_null($gestion->estado) ? 'No Aplica' : $gestion->estado}}</th>
                <th>{{ is_null($gestion->usuario_generador) ? 'No Aplica' : $gestion->usuario_generador}}</th>
                <th>{{ is_null($gestion->usuario_asignado) ? 'No Aplica' : $gestion->usuario_asignado}}</th>
                <th>{{ is_null($gestion->observaciones) ? 'No Aplica' : $gestion->observaciones}}</th>
                <th>{{ is_null($gestion->respuesta) ? 'No Aplica' : $gestion->respuesta}}</th>
                <th>{{ is_null($gestion->nombre_cliente) ? 'No Aplica' : $gestion->nombre_cliente}}</th>
                <th>{{ is_null($gestion->motivo_solicitud) ? 'No Aplica' : $gestion->motivo_solicitud}}</th>
                <th>{{ is_null($gestion->min) ? 'No Aplica' : $gestion->min}}</th>
                <th>{{ is_null($gestion->fecha_comunicacion) ? 'No Aplica' : $gestion->fecha_comunicacion}}</th>
                <th>{{ is_null($gestion->custcode) ? 'No Aplica' : $gestion->custcode}}</th>
                <th>{{ is_null($gestion->user_ac) ? 'No Aplica' : $gestion->user_ac}}</th>
                <th>{{ is_null($gestion->descripcion_propuesta) ? 'No Aplica' : $gestion->descripcion_propuesta}}</th>
                <th>{{ is_null($gestion['tipificacion']['item']) ? 'No Aplica' : $gestion['tipificacion']['item']}}</th>
                <th>{{ is_null($gestion->motivo) ? 'No Aplica' : $gestion->motivo}}</th>
                <th>{{ is_null($gestion->usuario->nombre) ? 'No Aplica' : $gestion->usuario->nombre}}</th>
            </tr>
        @endforeach
    </tbody>
</table>