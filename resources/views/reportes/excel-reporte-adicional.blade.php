<table>
    
    <thead>
        <tr>
            <td colspan="3" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada y Usuario') }}</td>          
        </tr>
        <tr>
            <td>min</td>
            <td>tipo de Gestion</td>
            <td>Usuario</td>            
        </tr>
    </thead>
    <tbody>
        @foreach ($gestiones as $gestion)  
            <tr>
                <th>{{ is_null($gestion->min) ? 'No Aplica' : $gestion->min}}</th>
                <th>{{ is_null($gestion->tipo_de_gestion) ? 'No Aplica' : $gestion->tipo_de_gestion}}</th>                  
                <th>{{ is_null($gestion['usuario']['nombre']) ? 'No Aplica' : $gestion['usuario']['nombre']}}</th>            
            </tr>
        @endforeach
    </tbody>
</table>