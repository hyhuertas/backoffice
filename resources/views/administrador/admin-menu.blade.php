@php
    $rolusu=Session::get('rolusu');
    $idusu=Session::get('idusu');
    $crm=Session::get('crm');
@endphp
<v-navigation-drawer class="menu_principal" v-model="drawer" :clipped="$vuetify.breakpoint.lgAndUp" app dark>
    <v-list dense>
      <template>
        <v-list-item key="1" @click="" href="{{ route('home', ['idusu'=>/* Crypt::encrypt($idusu) */$idusu,'crm'=>$crm]) }}">
            <v-list-item-icon>
              <v-icon large>home</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Inicio</v-list-item-title>
          </v-list-item>
          <v-list-item key="2" @click="" href="{{route('cargar-excel-admin',/* Crypt::encrypt('idusu','crm') */compact('idusu', 'crm'))}}">
            <v-list-item-icon>
              <v-icon large> cloud_upload</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Cargue Excel</v-list-item-title>
          </v-list-item>
          <v-list-item key="3" @click="" href="{{route('descargar-excel',[/* Crypt::encrypt('idusu',$idusu) */'idusu'=>$idusu,'crm'=>$crm])}}">
            <v-list-item-icon>
              <v-icon large>dynamic_feed</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Descargue Excel</v-list-item-title>
          </v-list-item>
          <v-list-item key="4" @click="" href="{{route('listar-usuarios',[/* Crypt::encrypt('idusu',$idusu) */'idusu'=>$idusu,'crm'=>$crm])}}">
            <v-list-item-icon>
              <v-icon large>how_to_reg</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Usuarios CRM</v-list-item-title>
          </v-list-item>
          <v-list-item key="5" @click="" href="{{route('cargar-excel-admin-adicional',/* Crypt::encrypt('idusu','crm') */compact('idusu', 'crm'))}}">
            <v-list-item-icon>
              <v-icon large>unarchive</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Cargue Adicional</v-list-item-title>
          </v-list-item>
          <v-list-item key="6" @click="" href="{{route('descargar-excel-adicional',[/* Crypt::encrypt('idusu',$idusu) */'idusu'=>$idusu,'crm'=>$crm])}}">
            <v-list-item-icon>
              <v-icon large>mdi-progress-download</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Descargue Adicionales</v-list-item-title>
          </v-list-item>
          <v-list-item key="7" @click="" href="{{route('listarPlanes',[/* Crypt::encrypt('idusu',$idusu) */'idusu'=>$idusu,'crm'=>$crm])}}">
            <v-list-item-icon>
              <v-icon large>mdi-monitor-cellphone-star</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Planes</v-list-item-title>
          </v-list-item>
        <v-divider  dark class="my-3"></v-divider>
      </template>
    </v-list>
</v-navigation-drawer>
