@extends('layout')
@section('componentes_vue')
<v-content>
    <v-container fluid fill-height class="pa-0 " style="max-width:100%" >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
                <admin-cargar-excel-adicional :rol_id={{auth()->user()->rol_user_id}}></admin-cargar-excel-adicional>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
    
    @include('tema_principal.bar')
     @include('administrador.admin-menu') 
       
@endsection