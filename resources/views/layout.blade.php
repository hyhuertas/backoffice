<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@3.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap" rel="stylesheet">
    
    @stack('styles')

    <title>Backoffice</title>
    <style>
        *{
            font-family: 'Fjalla One', sans-serif;
        }
    </style>
</head>

<body>
    <div id="app">
        <v-app id="inspire">
                <v-img src="/img/background.png">
              @yield('componentes_vue')
            </v-img>
           
        </v-app>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    
   
    
    @stack('scripts')
</body>
</html>
