@extends('layout')
@section('componentes_vue')
<v-content>
    <v-container fluid fill-height class="pa-0 " style="max-width:100%" >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
                <backoffice-estadisticas :rol_id={{auth()->user()->rol_user_id }}></backoffice-estadisticas>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
    @include('tema_principal.bar')
    @include('backofficeRetenido.backoffice-retenido-menu')
@endsection
