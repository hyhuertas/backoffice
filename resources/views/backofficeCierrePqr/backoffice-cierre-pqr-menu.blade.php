@php
    $rolusu=Session::get('rolusu');
    $idusu=Session::get('idusu');
    $crm=Session::get('crm');
@endphp
<v-navigation-drawer class="menu_principal" v-model="drawer" :clipped="$vuetify.breakpoint.lgAndUp" app dark>
    <v-list dense>
      <template>
        <v-list-item key="1" @click="" href="{{ route('home', ['opc'=>7,'idusu'=>$idusu,'crm'=>$crm]) }}">
            <v-list-item-icon>
              <v-icon large>home</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Inicio</v-list-item-title>
          </v-list-item>
          <v-list-item key="2" @click="" href="{{route('cargar-excel',compact('idusu','crm'))}}">
            <v-list-item-icon>
              <v-icon large>dynamic_feed</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Cargue Excel</v-list-item-title>
          </v-list-item>
          <v-list-item key="3" @click="" href="{{route('ver-gestiones',compact('idusu','crm'))}}">
            <v-list-item-icon>
              <v-icon large>transfer_within_a_station</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Gestiones</v-list-item-title>
          </v-list-item>
          <v-list-item key="4" @click="" href="{{route('ver-gestion-estado',['opc'=>7,'idusu'=>$idusu,'crm'=>$crm])}}">
            <v-list-item-icon>
              <v-icon large>how_to_reg</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Mis Gestionados</v-list-item-title>
          </v-list-item>
          <v-list-item key="5" @click="" href="{{route('cargar-excel-adicional',compact('idusu','crm'))}}">
            <v-list-item-icon>
              <v-icon large>unarchive</v-icon>
            </v-list-item-icon>
            <v-list-item-title class="subtitle-1">Excel Adicional</v-list-item-title>
          </v-list-item>
        <v-divider  dark class="my-3"></v-divider>
      </template>
    </v-list>
</v-navigation-drawer>
