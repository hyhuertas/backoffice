require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

window.Vue = require('vue');


import Vue from 'vue'
import Vuetify from 'vuetify'




Vue.use(Vuetify)


Vue.component('backoffice-listar-gestion', require('./components/backoffice/BackofficeListarGestion.vue').default);
Vue.component('backoffice-listar-gestion-estado', require('./components/backoffice/BackofficeListarGestionEstado.vue').default);
Vue.component('backoffice-cargar-excel', require('./components/backoffice/BackofficeCargarExcel.vue').default);
Vue.component('admin-cargar-excel', require('./components/administrador/AdminCargueExcel.vue').default);
Vue.component('backoffice-correo', require('./components/backoffice/BackofficeCorreo.vue').default);
Vue.component('admin-listar-usuarios', require('./components/administrador/AdminListarUsuarios.vue').default);
Vue.component('admin-descargar-excel', require('./components/administrador/AdminDescargarExcel.vue').default);
Vue.component('admin-cargar-excel-adicional', require('./components/administrador/AdminCargueExcelAdicional.vue').default);
Vue.component('backoffice-cargar-excel-adicional', require('./components/backoffice/BackofficeCargarExcelAdicional.vue').default);
Vue.component('admin-descargar-excel-adicional', require('./components/administrador/AdminDescargarExcelAdicional.vue').default);
Vue.component('backoffice-estadisticas', require('./components/estadisticas/BackofficeEstadisticas.vue').default);
Vue.component('admin-planes', require('./components/administrador/AdminPlanes.vue').default);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({
        iconfont: 'md',
        theme: {
            themes: {
                light: {
                    primary: '#0054a5',
                    secondary: '#6606D2',
                    accent: '#82B1FF',
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    data() {
        return {
            dialog: false,
            drawer: null,
            hints: true,
        }
    },
});